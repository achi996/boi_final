from rest_framework import serializers

from content_en.models import *


class FAQEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = FAQEn
        fields = '__all__'


class TeamMemberEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamMemberEn
        fields = '__all__'


class MissionEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = MissionEn
        fields = '__all__'


class NewsEnImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsEnImage
        fields = ('id', 'image')


class NewsEnPDFSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsEnPDF
        fields = ('id', 'file')


class NewsVideoEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsVideoEn
        fields = ('id', 'title', 'link')


class NewsEnSerializer(serializers.ModelSerializer):
    images = NewsEnImageSerializer(many=True, read_only=True)
    videos = NewsVideoEnSerializer(many=True, read_only=True)
    pdfs = NewsEnPDFSerializer(many=True, read_only=True)

    class Meta:
        model = NewsEn
        fields = '__all__'
        extra_fields = ['images', 'videos', 'pdfs']


class CaseEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaseEn
        fields = '__all__'


class AnnualReportEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnualReportEn
        fields = '__all__'


class QuarterReportEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuarterReportEn
        fields = '__all__'

class PurposeEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurposeEn
        fields = '__all__'


class PrincipleEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = PrincipleEn
        fields = '__all__'


class MediaEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaEn
        fields = '__all__'


class SmiEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmiEn
        fields = '__all__'


class NormativeDocEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = NormativeDocEn
        fields = '__all__'


class ContactInfoEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactInfoEn
        fields = '__all__'


class SmiVideoEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmiVideoEn
        fields = '__all__'


class AnalyticEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalyticEn
        fields = '__all__'

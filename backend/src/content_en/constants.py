REPORT_TYPE = (
    ('KV', 'Quarterly'),
    ('YE', 'Yearly'),
    ('OT', 'Other'),
)

QUARTER_TYPE = (
    ('I', "1st quarter"),
    ('II', "2nd quarter"),
    ('III', "3rd quarter"),
    ('IV', "4th quarter")
)
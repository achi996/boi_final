from ckeditor.fields import RichTextField
from django.db import models
from django.utils import timezone

from utils import generate_image_filename, SingletonModel, generate_file_filename
from content_en import constants

from common.models import generate_document_filename


class FAQEn(models.Model):
    question = models.TextField('Question')
    answer = models.TextField('Answer')

    class Meta:
        verbose_name = 'FAQ EN'
        verbose_name_plural = 'FAQ EN'
        ordering = ('id',)

    def __str__(self):
        return self.question


class TeamMemberEn(models.Model):
    full_name = models.CharField(
        verbose_name='Full name',
        max_length=255
    )
    position = models.CharField(
        'Position',
        max_length=255
    )
    image = models.ImageField(
        'Photo',
        upload_to=generate_image_filename
    )
    bio = models.TextField('Bio')

    class Meta:
        verbose_name = 'Member'
        verbose_name_plural = 'Members'
        ordering = ['id']

    def __str__(self):
        return self.full_name


class MissionEn(SingletonModel):
    mission = models.TextField('Mission')
    image = models.ImageField(
        verbose_name='Photo',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.mission

    class Meta:
        verbose_name = 'Mission'
        verbose_name_plural = 'Missions'
        ordering = ['id']


class NewsEn(models.Model):
    title = models.CharField('Title', max_length=255)
    description = RichTextField(verbose_name='Description')
    date = models.DateField(
        verbose_name='Date',
        default=timezone.now
    )
    is_main = models.BooleanField('Main new', default=False)
    kg_version = models.OneToOneField('content_kg.NewsKg', on_delete=models.CASCADE, verbose_name='Kyrgyz', blank=True, null=True)
    ru_version = models.OneToOneField('content_ru.NewsRu', on_delete=models.CASCADE, verbose_name='Russian', blank=True, null=True)

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'
        ordering = ('-date',)

    def __str__(self):
        return self.title


class NewsEnImage(models.Model):
    news = models.ForeignKey(NewsEn, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=generate_image_filename, blank=True,  null=True)


class NewsVideoEn(models.Model):
    news = models.ForeignKey(NewsEn, on_delete=models.CASCADE, related_name='videos')
    link = models.TextField(null=True, verbose_name='Iframe oт youtube')
    title = models.CharField(max_length=255, verbose_name='Title', null=True)

    def __str__(self):
        return self.title


class NewsEnPDF(models.Model):
    news = models.ForeignKey(NewsEn, on_delete=models.CASCADE, related_name='pdfs')
    file = models.FileField(upload_to=generate_file_filename, blank=True,  null=True)


class CaseEn(models.Model):
    title = models.CharField('Case', max_length=255)
    description = RichTextField(verbose_name='Description')
    date = models.DateField('Date', default=timezone.now)
    month = models.PositiveIntegerField('Month')
    year = models.PositiveIntegerField('Year')

    class Meta:
        verbose_name = 'Case'
        verbose_name_plural = 'Cases'
        ordering = ['-date']

    def __str__(self):
        return self.title


class AnnualReportEn(models.Model):
    title = models.CharField('Report name', max_length=255)
    file = models.FileField('File', upload_to=generate_image_filename)
    date = models.DateField('Date')
    year = models.PositiveIntegerField('Year')

    class Meta:
        verbose_name = 'Annual report'
        verbose_name_plural = 'Annual reports'
        ordering = ['-date']

    def __str__(self):
        return f"{self.id}-{self.title}"


class QuarterReportEn(models.Model):
    title = models.CharField('Report name', max_length=255)
    quarter_type = models.CharField(
        'Quarter', max_length=255, choices=constants.QUARTER_TYPE
    )
    file = models.FileField('File', upload_to=generate_image_filename)
    date = models.DateField('Date')
    year = models.PositiveIntegerField('Year')

    class Meta:
        verbose_name = 'Quarter report'
        verbose_name_plural = 'Quarter reports'
        ordering = ['-date']

    def __str__(self):
        return f"{self.id}-{self.title}"


class PurposeEn(models.Model):
    title = models.TextField('Purpose')
    image = models.ImageField(
        'Image',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Purpose'
        verbose_name_plural = 'Purposes'
        ordering = ['id']


class PrincipleEn(models.Model):
    title = models.CharField('Principle', max_length=255)
    image = models.ImageField(
        'Image',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Principle'
        verbose_name_plural = 'Principle'
        ordering = ['id']


class MediaEn(models.Model):
    title = models.CharField('Title', max_length=255)
    description = RichTextField(verbose_name='Description')
    date = models.DateField(
        verbose_name='Date',
        default=timezone.now
    )
    image = models.ImageField(
        verbose_name='Date',
        upload_to=generate_image_filename,
        blank=True, null=True
    )

    class Meta:
        verbose_name = 'Media'
        verbose_name_plural = 'Media'
        ordering = ['id']
    
    def __str__(self):
        return self.title


class SmiEn(models.Model):
    title = models.CharField('Title', max_length=255)
    description = RichTextField(verbose_name='Description')
    date = models.DateField(
        verbose_name='Date',
        default=timezone.now
    )
    image = models.ImageField(
        verbose_name='Image',
        upload_to=generate_image_filename,
        blank=True, null=True
    )

    class Meta:
        verbose_name = 'Mass Media Event'
        verbose_name_plural = 'Mass Media Events'
        ordering = ('-date',)

    def __str__(self):
        return self.title


class SmiVideoEn(models.Model):
    smi = models.ForeignKey(SmiEn, on_delete=models.CASCADE, related_name='videos')
    link = models.TextField(null=True, verbose_name='Iframe oт youtube')
    title = models.CharField(max_length=255, verbose_name='Title', null=True)

    def __str__(self):
        return self.title


class NormativeDocEn(models.Model):
    title = models.CharField(max_length=255, verbose_name="Title")
    document = models.FileField(
        upload_to=generate_document_filename, blank=True, null=True
    )

    class Meta:
        verbose_name = 'Normative Document'
        verbose_name_plural = 'Normative Documents'
        ordering = ('-id',)


class ContactInfoEn(models.Model):
    street = models.CharField(max_length=255, null=True)
    number = models.CharField(max_length=255, null=True, verbose_name='Number of building')
    office = models.CharField(max_length=255, null=True, verbose_name='Business Center/Office/Building')
    city = models.CharField(max_length=255, null=True)
    zip_code = models.CharField(max_length=255, null=True)
    phone1 = models.CharField(max_length=255, null=True)
    phone2 = models.CharField(max_length=255, null=True)

    class Meta:
        verbose_name = 'Contact Info'
        verbose_name_plural = 'Contact Infos'


class AnalyticEn(models.Model):
    title = models.CharField('Title', max_length=255, null=True)
    description = RichTextField(verbose_name='Description', null=True)
    date = models.DateField(
        verbose_name='Date',
        default=timezone.now
    )
    file = models.FileField('File', upload_to=generate_image_filename, null=True, blank=True)
    kg_version = models.OneToOneField('content_kg.AnalyticKg', on_delete=models.CASCADE, verbose_name='Kyrgyz', blank=True, null=True)
    ru_version = models.OneToOneField('content_ru.AnalyticRu', on_delete=models.CASCADE, verbose_name='Russian', blank=True, null=True)

    class Meta:
        verbose_name = 'Analytics'
        verbose_name_plural = 'Analytics'
        ordering = ('-date',)

    def __str__(self):
        return self.title

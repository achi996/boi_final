from rest_framework import viewsets
from rest_framework.decorators import api_view
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Q

from rest_framework.response import Response

from content_en.serializers import *


class FAQEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FAQEn.objects.all()
    serializer_class = FAQEnSerializer


class TeamMemberEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TeamMemberEn.objects.all()
    serializer_class = TeamMemberEnSerializer


class MissionEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MissionEn.objects.all()
    serializer_class = MissionEnSerializer


class NewsEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsEn.objects.all()
    serializer_class = NewsEnSerializer


class CaseEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CaseEn.objects.all()
    serializer_class = CaseEnSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['date', 'year', 'month']


class AnnualReportEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AnnualReportEn.objects.all()
    serializer_class = AnnualReportEnSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['date', ]

class QuarterReportEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = QuarterReportEn.objects.all()
    serializer_class = QuarterReportEnSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['year', 'quarter_type']

class PurposeEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PurposeEn.objects.all()
    serializer_class = PurposeEnSerializer


class PrincipleEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PrincipleEn.objects.all()
    serializer_class = PrincipleEnSerializer


class MediaEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MediaEn.objects.all()
    serializer_class = MediaEnSerializer


class SmiEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SmiEn.objects.all()
    serializer_class = SmiEnSerializer


class NewsEnImageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsEnImage.objects.all()
    serializer_class = NewsEnImageSerializer


class NormativeDocEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NormativeDocEn.objects.all()
    serializer_class = NormativeDocEnSerializer


class ContactInfoEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ContactInfoEn.objects.all()
    serializer_class = ContactInfoEnSerializer


class NewsVideoEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsVideoEn.objects.all()
    serializer_class = NewsEnPDFSerializer


class NewsEnPDFViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsEnPDF.objects.all()
    serializer_class = NewsEnPDFSerializer


class SmiVideoEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SmiVideoEn.objects.all()
    serializer_class = SmiVideoEnSerializer


class AnalyticEnViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AnalyticEn.objects.all()
    serializer_class = AnalyticEnSerializer


@api_view(['GET'])
def search_all_models(request):
    q = request.GET.get('q')
    news = NewsEn.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    cases = CaseEn.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    an_reps = AnnualReportEn.objects.filter(Q(title__icontains=q))
    q_reps = QuarterReportEn.objects.filter(Q(title__icontains=q))
    docs = NormativeDocEn.objects.filter(Q(title__icontains=q))
    anals = AnalyticEn.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    faqs = FAQEn.objects.filter(Q(question__icontains=q) | Q(answer__icontains=q))
    team = TeamMemberEn.objects.filter(Q(full_name__icontains=q))
    data = {
        'news': NewsEnSerializer(news, many=True, allow_null=True).data,
        'cases': CaseEnSerializer(cases, many=True, allow_null=True).data,
        'annual_reports': AnnualReportEnSerializer(an_reps, many=True, allow_null=True).data,
        'quarter_reports': QuarterReportEnSerializer(q_reps, many=True, allow_null=True).data,
        'normative_docs': NormativeDocEnSerializer(docs, many=True, allow_null=True).data,
        'analytics': AnalyticEnSerializer(anals, many=True, allow_null=True).data,
        'faq': FAQEnSerializer(faqs, many=True, allow_null=True).data,
        'team_members': TeamMemberEnSerializer(team, many=True, allow_null=True).data
    }
    return Response(data)

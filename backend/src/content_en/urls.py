from rest_framework.routers import DefaultRouter
from django.urls import path
from content_en import views


router = DefaultRouter()

router.register('faq', views.FAQEnViewSet)
router.register('team', views.TeamMemberEnViewSet)
router.register('missions', views.MissionEnViewSet)
router.register('news', views.NewsEnViewSet)
router.register('cases', views.CaseEnViewSet)
router.register('annual-reports', views.AnnualReportEnViewSet)
router.register('quarter-reports', views.QuarterReportEnViewSet)
router.register('purposes', views.PurposeEnViewSet)
router.register('principles', views.PrincipleEnViewSet)
router.register('media', views.MediaEnViewSet)
router.register('smi', views.SmiEnViewSet)
router.register('news-images', views.NewsEnImageViewSet)
router.register('normative-documents', views.NormativeDocEnViewSet)
router.register('contact-info', views.ContactInfoEnViewSet)
router.register('news-video', views.NewsVideoEnViewSet)
router.register('news-pdf', views.NewsEnPDFViewSet)
router.register('smi-video', views.SmiVideoEnViewSet)
router.register('analytics', views.AnalyticEnViewSet)

urlpatterns = [
    path('search/', views.search_all_models, name='search_kg')
]

urlpatterns += router.urls

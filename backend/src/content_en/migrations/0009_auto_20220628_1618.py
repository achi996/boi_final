# Generated by Django 3.1.2 on 2022-06-28 10:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('content_kg', '0008_analytickg_ru_version'),
        ('content_ru', '0007_auto_20220418_0228'),
        ('content_en', '0008_auto_20220418_0228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='analyticen',
            name='kg_version',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='content_kg.analytickg', verbose_name='Kyrgyz'),
        ),
        migrations.AlterField(
            model_name='analyticen',
            name='ru_version',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='content_ru.analyticru', verbose_name='Russian'),
        ),
        migrations.AlterField(
            model_name='newsen',
            name='kg_version',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='content_kg.newskg', verbose_name='Kyrgyz'),
        ),
        migrations.AlterField(
            model_name='newsen',
            name='ru_version',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='content_ru.newsru', verbose_name='Russian'),
        ),
    ]

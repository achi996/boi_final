# Generated by Django 3.1.2 on 2021-12-09 10:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('content_ru', '0004_smivideoru'),
        ('content_kg', '0004_smivideokg'),
        ('content_en', '0004_smivideoen'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsen',
            name='kg_version',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='content_kg.newskg', verbose_name='Kyrgyz version'),
        ),
        migrations.AlterField(
            model_name='newsen',
            name='ru_version',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='content_ru.newsru', verbose_name='Russion verion'),
        ),
    ]

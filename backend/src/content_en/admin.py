from django.contrib import admin

from common.admin import (
    CaseAdmin,
    AnnualReportAdmin,
    QuarterReportAdmin,
    FAQAdmin,
    TeamMemberAdmin
)
from .models import *


class NewsEnImageInline(admin.TabularInline):
    model = NewsEnImage
    extra = 0


class NewsVideoEnInline(admin.TabularInline):
    model = NewsVideoEn
    extra = 0


class NewsEnPDFInline(admin.TabularInline):
    model = NewsEnPDF
    extra = 0


class SmiVideoEnInline(admin.TabularInline):
    model = SmiVideoEn
    extra = 0


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'date')
    inlines = [NewsEnImageInline, NewsVideoEnInline, NewsEnPDFInline]


class SmiEnAdmin(admin.ModelAdmin):
    inlines = [SmiVideoEnInline]


admin.site.register(NewsEn, NewsAdmin)
admin.site.register(CaseEn, CaseAdmin)
admin.site.register(AnnualReportEn, AnnualReportAdmin)
admin.site.register(QuarterReportEn, QuarterReportAdmin)
admin.site.register(FAQEn, FAQAdmin)
admin.site.register(TeamMemberEn, TeamMemberAdmin)
admin.site.register(MissionEn)
admin.site.register(PurposeEn)
# admin.site.register(PrincipleEn)
admin.site.register(MediaEn)
admin.site.register(SmiEn, SmiEnAdmin)
admin.site.register(NormativeDocEn)
admin.site.register(ContactInfoEn)
admin.site.register(AnalyticEn)

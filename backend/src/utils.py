import os
import uuid
from django.db import models


def generate_image_filename(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = f'{str(uuid.uuid4())}.{ext}'
    return os.path.join('images', new_filename)


def generate_file_filename(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = f'{str(uuid.uuid4())}.{ext}'
    return os.path.join('pdfs', new_filename)


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj
from ckeditor.fields import RichTextField
from django.db import models
from django.utils import timezone

from utils import generate_image_filename, SingletonModel, generate_file_filename
from content_kg import constants

from common.models import generate_document_filename


class FAQKg(models.Model):
    question = models.TextField('Суроо')
    answer = models.TextField('Жообу')

    class Meta:
        verbose_name = 'FAQ KG'
        verbose_name_plural = 'FAQ KG'
        ordering = ('id',)

    def __str__(self):
        return self.question


class TeamMemberKg(models.Model):
    full_name = models.CharField(
        verbose_name='Аты-жөну',
        max_length=255
    )
    position = models.CharField(
        'Ээлеген кызматы',
        max_length=255
    )
    image = models.ImageField(
        'Сүрөтү',
        upload_to=generate_image_filename
    )
    bio = models.TextField('Маалымат')

    class Meta:
        verbose_name = 'Кызматкер'
        verbose_name_plural = 'Кызматкерлер'
        ordering = ['id']

    def __str__(self):
        return self.full_name


class MissionKg(SingletonModel):
    mission = models.TextField('Миссия')
    image = models.ImageField(
        verbose_name='Сүрөтү',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.mission

    class Meta:
        verbose_name = 'Миссия'
        verbose_name_plural = 'Миссиялар'
        ordering = ['id']


class NewsKg(models.Model):
    title = models.CharField('Аты', max_length=255)
    description = RichTextField(verbose_name='Сүрөттөмөсү')
    date = models.DateField(
        verbose_name='Датасы',
        default=timezone.now
    )
    is_main = models.BooleanField('Башкы жанылык', default=False)
    ru_version = models.OneToOneField('content_ru.NewsRu', on_delete=models.CASCADE, verbose_name='Орусча', blank=True, null=True)
    en_version = models.OneToOneField('content_en.NewsEn', on_delete=models.CASCADE, verbose_name='Англисче', blank=True, null=True)

    class Meta:
        verbose_name = 'Жанылык'
        verbose_name_plural = 'Жанылыктар'
        ordering = ('-date',)

    def __str__(self):
        return self.title


class NewsKgImage(models.Model):
    news = models.ForeignKey(NewsKg, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=generate_image_filename, blank=True,  null=True)


class NewsKgPDF(models.Model):
    news = models.ForeignKey(NewsKg, on_delete=models.CASCADE, related_name='pdfs')
    file = models.FileField(upload_to=generate_file_filename, blank=True,  null=True)


class NewsVideoKg(models.Model):
    news = models.ForeignKey(NewsKg, on_delete=models.CASCADE, related_name='videos')
    link = models.TextField(null=True, verbose_name='Iframe oт youtube')
    title = models.CharField(max_length=255, verbose_name='Видеонун аты', null=True)

    def __str__(self):
        return self.title


class CaseKg(models.Model):
    title = models.CharField('Иш', max_length=255)
    description = RichTextField(verbose_name='Сүрөттөмөсү')
    date = models.DateField('Датасы', default=timezone.now)
    month = models.PositiveIntegerField('Айы')
    year = models.PositiveIntegerField('Жылы')

    class Meta:
        verbose_name = 'Кейс'
        verbose_name_plural = 'Кейстер'
        ordering = ['-date']

    def __str__(self):
        return self.title


class AnnualReportKg(models.Model):
    title = models.CharField('Отчёттун аталышы', max_length=255)
    file = models.FileField('Файл', upload_to=generate_image_filename)
    date = models.DateField('Дата', default=timezone.now)
    year = models.PositiveIntegerField('Жылы')

    class Meta:
        verbose_name = 'Жылдык отчёт'
        verbose_name_plural = 'Жылдык отчёттор'
        ordering = ['-date']

    def __str__(self):
        return f"{self.id}-{self.title}"


class QuarterReportKg(models.Model):
    title = models.CharField('Отчёттун аталышы', max_length=255)
    quarter_type = models.CharField(
        'Квартал', max_length=255, choices=constants.QUARTER_TYPE
    )
    file = models.FileField('Файл', upload_to=generate_image_filename)
    date = models.DateField('Дата', default=timezone.now)
    year = models.PositiveIntegerField('Жылы')

    class Meta:
        verbose_name = 'Кварталдык отчёт'
        verbose_name_plural = 'Кварталдык отчёттор'
        ordering = ['-date']

    def __str__(self):
        return f"{self.id}-{self.title}"


class PurposeKg(models.Model):
    title = models.TextField('Максат')
    image = models.ImageField(
        'Сүрөтү',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Максат'
        verbose_name_plural = 'Макстатар'
        ordering = ['id']


class PrincipleKg(models.Model):
    title = models.CharField('Аты', max_length=255)
    image = models.ImageField(
        'Сүрөтү',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Принцип'
        verbose_name_plural = 'Принциптер'
        ordering = ['id']


class MediaKg(models.Model):
    title = models.CharField('Аты', max_length=255)
    description = RichTextField(verbose_name='Сүрөттөмөсү')
    date = models.DateField(
        verbose_name='Датасы',
        default=timezone.now
    )
    image = models.ImageField(
        verbose_name='Фото',
        upload_to=generate_image_filename,
        blank=True, null=True
    )

    class Meta:
        verbose_name = 'Медиа'
        verbose_name_plural = 'Медиа'
        ordering = ['id']

    def __str__(self):
        return self.title


class SmiKg(models.Model):
    title = models.CharField('Аты', max_length=255)
    description = RichTextField(verbose_name='Сүрөттөмөсү')
    date = models.DateField(
        verbose_name='Датасы',
        default=timezone.now
    )
    image = models.ImageField(
        verbose_name='Фото',
        upload_to=generate_image_filename,
        blank=True, null=True
    )

    class Meta:
        verbose_name = 'СМИ'
        verbose_name_plural = 'СМИ'
        ordering = ('-date',)

    def __str__(self):
        return self.title


class SmiVideoKg(models.Model):
    smi = models.ForeignKey(SmiKg, on_delete=models.CASCADE, related_name='videos')
    link = models.TextField(null=True, verbose_name='Iframe oт youtube')
    title = models.CharField(max_length=255, verbose_name='Title', null=True)

    def __str__(self):
        return self.title


class NormativeDocKg(models.Model):
    title = models.CharField(max_length=255, verbose_name="Аты")
    document = models.FileField(
        upload_to=generate_document_filename, blank=True, null=True
    )

    class Meta:
        verbose_name = 'Нормативдуу документ'
        verbose_name_plural = 'Нормативдуу документтер'
        ordering = ('-id',)


class ContactInfoKg(models.Model):
    street = models.CharField(max_length=255, null=True, verbose_name='Кочоо')
    number = models.CharField(max_length=255, null=True, verbose_name='Имараттын номери')
    office = models.CharField(max_length=255, null=True, verbose_name='Имараттын аты')
    city = models.CharField(max_length=255, null=True, verbose_name='Шаар')
    zip_code = models.CharField(max_length=255, null=True, verbose_name='ZIP')
    phone1 = models.CharField(max_length=255, null=True, verbose_name='Биринчи телефон')
    phone2 = models.CharField(max_length=255, null=True, verbose_name='Экинчи телефон')

    class Meta:
        verbose_name = 'Байланышуу'
        verbose_name_plural = 'Байланышуу'


class AnalyticKg(models.Model):
    title = models.CharField('Аты', max_length=255, null=True)
    description = RichTextField(verbose_name='Мазмуну', null=True)
    date = models.DateField(
        verbose_name='Датасы',
        default=timezone.now
    )
    file = models.FileField('Файл', upload_to=generate_image_filename, null=True, blank=True)
    en_version = models.OneToOneField('content_en.AnalyticEn', on_delete=models.CASCADE, verbose_name='Англисче', blank=True, null=True)
    ru_version = models.OneToOneField('content_ru.AnalyticRu', on_delete=models.CASCADE, verbose_name='Орусча', blank=True, null=True)

    class Meta:
        verbose_name = 'Аналитика'
        verbose_name_plural = 'Аналитикалар'
        ordering = ('-date',)

    def __str__(self):
        return self.title

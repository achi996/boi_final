from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response

from content_kg.serializers import *


class FAQKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FAQKg.objects.all()
    serializer_class = FAQKgSerializer


class TeamMemberKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TeamMemberKg.objects.all()
    serializer_class = TeamMemberKgSerializer


class MissionKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MissionKg.objects.all()
    serializer_class = MissionKgSerializer


class NewsKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsKg.objects.all()
    serializer_class = NewsKgSerializer


class CaseKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CaseKg.objects.all()
    serializer_class = CaseKgSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['date', 'year', 'month']


class AnnualReportKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AnnualReportKg.objects.all()
    serializer_class = AnnualReportKgSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['date',]

class QuarterReportKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = QuarterReportKg.objects.all()
    serializer_class = QuarterReportKgSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['year', 'quarter_type',]

class PurposeKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PurposeKg.objects.all()
    serializer_class = PurposeKgSerializer


class PrincipleKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PrincipleKg.objects.all()
    serializer_class = PrincipleKgSerializer


class MediaKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MediaKg.objects.all()
    serializer_class = MediaKgSerializer


class SmiKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SmiKg.objects.all()
    serializer_class = SmiKgSerializer


class NewsKgImageViewSet(
     mixins.RetrieveModelMixin,
     mixins.ListModelMixin,
     viewsets.GenericViewSet):
    queryset = NewsKgImage.objects.all()
    serializer_class = NewsKgImageSerializer


class NormativeDocKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NormativeDocKg.objects.all()
    serializer_class = NormativeDocKgSerializer


class ContactInfoKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ContactInfoKg.objects.all()
    serializer_class = ContactInfoKgSerializer


class NewsVideoKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsVideoKg.objects.all()
    serializer_class = NewsVideoKgSerializer


class NewsKgPDFViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsKgPDF.objects.all()
    serializer_class = NewsKgPDFSerializer


class SmiVideoKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SmiVideoKg.objects.all()
    serializer_class = SmiVideoKgSerializer


class AnalyticKgViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AnalyticKg.objects.all()
    serializer_class = AnalyticKgSerializer


@api_view(['GET'])
def search_all_models(request):
    q = request.GET.get('q')
    news = NewsKg.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    cases = CaseKg.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    an_reps = AnnualReportKg.objects.filter(Q(title__icontains=q))
    q_reps = QuarterReportKg.objects.filter(Q(title__icontains=q))
    docs = NormativeDocKg.objects.filter(Q(title__icontains=q))
    anals = AnalyticKg.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    faqs = FAQKg.objects.filter(Q(question__icontains=q) | Q(answer__icontains=q))
    team = TeamMemberKg.objects.filter(Q(full_name__icontains=q))
    data = {
        'news': NewsKgSerializer(news, many=True, allow_null=True).data,
        'cases': CaseKgSerializer(cases, many=True, allow_null=True).data,
        'annual_reports': AnnualReportKgSerializer(an_reps, many=True, allow_null=True).data,
        'quarter_reports': QuarterReportKgSerializer(q_reps, many=True, allow_null=True).data,
        'normative_docs': NormativeDocKgSerializer(docs, many=True, allow_null=True).data,
        'analytics': AnalyticKgSerializer(anals, many=True, allow_null=True).data,
        'faq': FAQKgSerializer(faqs, many=True, allow_null=True).data,
        'team_members': TeamMemberKgSerializer(team, many=True, allow_null=True).data
    }
    return Response(data)

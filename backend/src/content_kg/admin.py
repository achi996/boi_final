from django.contrib import admin

from common.admin import (
    CaseAdmin,
    AnnualReportAdmin,
    QuarterReportAdmin,
    FAQAdmin,
    TeamMemberAdmin
)
from .models import *


class NewsKgImageInline(admin.TabularInline):
    model = NewsKgImage
    extra = 0


class NewsVideoKgInline(admin.TabularInline):
    model = NewsVideoKg
    extra = 0


class NewsKgPDFInline(admin.TabularInline):
    model = NewsKgPDF
    extra = 0


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'date')
    inlines = [NewsKgImageInline, NewsVideoKgInline, NewsKgPDFInline]


class SmiVideoKgInline(admin.TabularInline):
    model = SmiVideoKg
    extra = 0


class SmiKgAdmin(admin.ModelAdmin):
    inlines = [SmiVideoKgInline]


admin.site.register(NewsKg, NewsAdmin)
admin.site.register(CaseKg, CaseAdmin)
admin.site.register(AnnualReportKg, AnnualReportAdmin)
admin.site.register(QuarterReportKg, QuarterReportAdmin)
admin.site.register(FAQKg, FAQAdmin)
admin.site.register(TeamMemberKg, TeamMemberAdmin)
admin.site.register(MissionKg)
admin.site.register(PurposeKg)
# admin.site.register(PrincipleKg)
admin.site.register(MediaKg)
admin.site.register(SmiKg, SmiKgAdmin)
admin.site.register(NormativeDocKg)
admin.site.register(ContactInfoKg)
admin.site.register(AnalyticKg)

admin.site.site_header = "Business Ombudsman Institute"
admin.site.site_title = "Business Ombudsman Institute ADMIN"
admin.site.index_title = "Welcome to BOI web-content Administration"

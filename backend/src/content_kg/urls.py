from django.urls import path
from rest_framework.routers import DefaultRouter

from content_kg import views


router = DefaultRouter()

router.register('faq', views.FAQKgViewSet)
router.register('team', views.TeamMemberKgViewSet)
router.register('missions', views.MissionKgViewSet)
router.register('news', views.NewsKgViewSet)
router.register('cases', views.CaseKgViewSet)
router.register('annual-reports', views.AnnualReportKgViewSet)
router.register('quarter-reports', views.QuarterReportKgViewSet)
router.register('purposes', views.PurposeKgViewSet)
router.register('principles', views.PrincipleKgViewSet)
router.register('media', views.MediaKgViewSet)
router.register('smi', views.SmiKgViewSet)
router.register('news-images', views.NewsKgImageViewSet)
router.register('normative-documents', views.NormativeDocKgViewSet)
router.register('contact-info', views.ContactInfoKgViewSet)
router.register('news-video', views.NewsVideoKgViewSet)
router.register('news-pdfs', views.NewsKgPDFViewSet)
router.register('smi-video', views.SmiVideoKgViewSet)
router.register('analytics', views.AnalyticKgViewSet)


urlpatterns = [
    path('search/', views.search_all_models, name='search_kg')
]

urlpatterns += router.urls

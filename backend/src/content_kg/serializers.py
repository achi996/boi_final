from rest_framework import serializers

from content_kg.models import *


class FAQKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = FAQKg
        fields = '__all__'


class TeamMemberKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamMemberKg
        fields = '__all__'


class MissionKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = MissionKg
        fields = '__all__'


class NewsKgPDFSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsKgPDF
        fields = ('id', 'file')


class NewsKgImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsKgImage
        fields = ('id', 'image')


class NewsVideoKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsVideoKg
        fields = ('id', 'title', 'link')


class NewsKgSerializer(serializers.ModelSerializer):
    images = NewsKgImageSerializer(many=True, read_only=True)
    videos = NewsVideoKgSerializer(many=True, read_only=True)
    pdfs = NewsKgPDFSerializer(many=True, read_only=True)

    class Meta:
        model = NewsKg
        fields = '__all__'
        extra_fields = ['images', 'videos', 'pdfs']


class CaseKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaseKg
        fields = '__all__'


class AnnualReportKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnualReportKg
        fields = '__all__'

class QuarterReportKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuarterReportKg
        fields = '__all__'

class PurposeKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurposeKg
        fields = '__all__'


class PrincipleKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = PrincipleKg
        fields = '__all__'


class MediaKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaKg
        fields = '__all__'


class SmiKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmiKg
        fields = '__all__'


class NormativeDocKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = NormativeDocKg
        fields = '__all__'


class ContactInfoKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactInfoKg
        fields = '__all__'


class SmiVideoKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmiVideoKg
        fields = '__all__'


class AnalyticKgSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalyticKg
        fields = '__all__'

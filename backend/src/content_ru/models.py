from ckeditor.fields import RichTextField
from django.db import models
from django.utils import timezone

from utils import generate_image_filename, SingletonModel, generate_file_filename
from content_ru import constants

from common.models import generate_document_filename


class FAQRu(models.Model):
    question = models.TextField('Вопрос')
    answer = models.TextField('Ответ')

    class Meta:
        verbose_name = 'FAQ RU'
        verbose_name_plural = 'FAQ RU'
        ordering = ('id',)

    def __str__(self):
        return self.question


class TeamMemberRu(models.Model):
    full_name = models.CharField(
        verbose_name='ФИО',
        max_length=255
    )
    position = models.CharField(
        'Должность',
        max_length=255
    )
    image = models.ImageField(
        'Фото',
        upload_to=generate_image_filename
    )
    bio = models.TextField('Описание')

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'
        ordering = ['id']

    def __str__(self):
        return self.full_name


class MissionRu(SingletonModel):
    mission = models.TextField('Миссия')
    image = models.ImageField(
        verbose_name='Фото',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.mission

    class Meta:
        verbose_name = 'Миссия'
        verbose_name_plural = 'Миссии'
        ordering = ['id']


class NewsRu(models.Model):
    title = models.CharField('Название', max_length=255)
    description = RichTextField(verbose_name='Описание')
    date = models.DateField(
        verbose_name='Дата',
        default=timezone.now
    )
    is_main = models.BooleanField('Главная новость', default=False)
    en_version = models.OneToOneField('content_en.NewsEn', on_delete=models.CASCADE, verbose_name='На английском', blank=True, null=True)
    kg_version = models.OneToOneField('content_kg.NewsKg', on_delete=models.CASCADE, verbose_name='На кыргызском', blank=True, null=True)

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ('-date',)

    def __str__(self):
        return self.title


class NewsRuImage(models.Model):
    news = models.ForeignKey(NewsRu, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=generate_image_filename, blank=True,  null=True)


class NewsRuPDF(models.Model):
    news = models.ForeignKey(NewsRu, on_delete=models.CASCADE, related_name='pdfs')
    file = models.FileField(upload_to=generate_file_filename, blank=True,  null=True)


class NewsVideoRu(models.Model):
    news = models.ForeignKey(NewsRu, on_delete=models.CASCADE, related_name='videos')
    link = models.TextField(null=True, verbose_name='Iframe oт youtube')
    title = models.CharField(max_length=255, verbose_name='Название видео', null=True)

    def __str__(self):
        return self.title


class CaseRu(models.Model):
    title = models.CharField('Кейс', max_length=255)
    description = RichTextField(verbose_name='Описание')
    date = models.DateField('Дата', default=timezone.now)
    month = models.PositiveIntegerField('Месяц')
    year = models.PositiveIntegerField('Год')

    class Meta:
        verbose_name = 'Кейс'
        verbose_name_plural = 'Кейсы'
        ordering = ['-date']

    def __str__(self):
        return self.title


class AnnualReportRu(models.Model):
    title = models.CharField('Название отчета', max_length=255)
    file = models.FileField('Файл', upload_to=generate_image_filename)
    date = models.DateField('Дата', default=timezone.now)
    year = models.PositiveIntegerField('Год')

    class Meta:
        verbose_name = 'Годовой отчет'
        verbose_name_plural = 'Годовые отчеты'
        ordering = ['-date']

    def __str__(self):
        return f"{self.id}-{self.title}"


class QuarterReportRu(models.Model):
    title = models.CharField('Название отчета', max_length=255)
    quarter_type = models.CharField(
        'Квартал', max_length=255, choices=constants.QUARTER_TYPE
    )
    file = models.FileField('Файл', upload_to=generate_image_filename)
    date = models.DateField('Дата', default=timezone.now)
    year = models.PositiveIntegerField('Год')

    class Meta:
        verbose_name = 'Квартальный отчет'
        verbose_name_plural = 'Квартальные отчеты'
        ordering = ['-date']

    def __str__(self):
        return f"{self.id}-{self.title}"


class PurposeRu(models.Model):
    title = models.TextField('Цель')
    image = models.ImageField(
        'Фото',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Цель'
        verbose_name_plural = 'Цели'
        ordering = ['id']


class PrincipleRu(models.Model):
    title = models.CharField('Принцип', max_length=255)
    image = models.ImageField(
        'Фото',
        upload_to=generate_image_filename
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Принцип'
        verbose_name_plural = 'Принципы'
        ordering = ['id']


class MediaRu(models.Model):
    title = models.CharField('Название', max_length=255)
    description = RichTextField(verbose_name='Описание')
    date = models.DateField(
        verbose_name='Дата',
        default=timezone.now
    )
    image = models.ImageField(
        verbose_name='Фото',
        upload_to=generate_image_filename,
        blank=True, null=True
    )

    class Meta:
        verbose_name = 'Media'
        verbose_name_plural = 'Media'
        ordering = ['-date']

    def __str__(self):
        return self.title


class SmiRu(models.Model):
    title = models.CharField('Название', max_length=255)
    description = RichTextField(verbose_name='Описание')
    date = models.DateField(
        verbose_name='Дата',
        default=timezone.now
    )
    image = models.ImageField(
        verbose_name='Фото',
        upload_to=generate_image_filename,
        blank=True, null=True
    )

    class Meta:
        verbose_name = 'СМИ о нас'
        verbose_name_plural = 'СМИ о нас'
        ordering = ('-date',)

    def __str__(self):
        return self.title


class SmiVideoRu(models.Model):
    smi = models.ForeignKey(SmiRu, on_delete=models.CASCADE, related_name='videos')
    link = models.TextField(null=True, verbose_name='Iframe oт youtube')
    title = models.CharField(max_length=255, verbose_name='Title', null=True)

    def __str__(self):
        return self.title


class NormativeDocRu(models.Model):
    title = models.CharField(max_length=255, verbose_name="Название")
    document = models.FileField(
        upload_to=generate_document_filename, blank=True, null=True
    )

    class Meta:
        verbose_name = 'Нормативный документ'
        verbose_name_plural = 'Нормативные документы'
        ordering = ('-id',)


class ContactInfoRu(models.Model):
    street = models.CharField(max_length=255, null=True, verbose_name='Улица')
    number = models.CharField(max_length=255, null=True, verbose_name='Номер здания')
    office = models.CharField(max_length=255, null=True, verbose_name='Название здания')
    city = models.CharField(max_length=255, null=True, verbose_name='Город')
    zip_code = models.CharField(max_length=255, null=True, verbose_name='ZIP')
    phone1 = models.CharField(max_length=255, null=True, verbose_name='Первый телефон')
    phone2 = models.CharField(max_length=255, null=True, verbose_name='Второй телефон')

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'


class AnalyticRu(models.Model):
    title = models.CharField('Аты', max_length=255, null=True)
    description = RichTextField(verbose_name='Мазмуну', null=True)
    date = models.DateField(
        verbose_name='Датасы',
        default=timezone.now
    )
    file = models.FileField('Файл', upload_to=generate_image_filename, null=True, blank=True)
    en_version = models.OneToOneField('content_en.AnalyticEn', on_delete=models.CASCADE, verbose_name='На английском', blank=True, null=True)
    kg_version = models.OneToOneField('content_kg.AnalyticKg', on_delete=models.CASCADE, verbose_name='На кыргызском', blank=True, null=True)

    class Meta:
        verbose_name = 'Аналитика'
        verbose_name_plural = 'Аналитика'
        ordering = ('-date',)

    def __str__(self):
        return self.title

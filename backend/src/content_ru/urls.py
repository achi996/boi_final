from django.urls import path
from rest_framework.routers import DefaultRouter

from content_ru import views


router = DefaultRouter()

router.register('faq', views.FAQRuViewSet)
router.register('team', views.TeamMemberRuViewSet)
router.register('missions', views.MissionRuViewSet)
router.register('news', views.NewsRuViewSet)
router.register('cases', views.CaseRuViewSet)
router.register('annual-reports', views.AnnualReportRuViewSet)
router.register('quarter-reports', views.QuarterReportRuViewSet)
router.register('purposes', views.PurposeRuViewSet)
router.register('principles', views.PrincipleRuViewSet)
router.register('media', views.MediaRuViewSet)
router.register('smi', views.SmiRuViewSet)
router.register('news-images', views.NewsRuImageViewSet)
router.register('normative-documents', views.NormativeDocRuViewSet)
router.register('contact-info', views.ContactInfoRuViewSet)
router.register('news-video', views.NewsVideoRuViewSet)
router.register('news-pdfs', views.NewsRuPDFViewSet)
router.register('smi-video', views.SmiVideoRuViewSet)
router.register('analytics', views.AnalyticRuViewSet)

urlpatterns = [
    path('search/', views.search_all_models, name='search_ru')
]

urlpatterns += router.urls

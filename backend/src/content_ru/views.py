from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response

from content_ru.serializers import *


class FAQRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FAQRu.objects.all()
    serializer_class = FAQRuSerializer


class TeamMemberRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TeamMemberRu.objects.all()
    serializer_class = TeamMemberRuSerializer


class MissionRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MissionRu.objects.all()
    serializer_class = MissionRuSerializer


class NewsRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsRu.objects.all()
    serializer_class = NewsRuSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['is_main',]

class CaseRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CaseRu.objects.all()
    serializer_class = CaseRuSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['date', 'year', 'month']


class AnnualReportRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AnnualReportRu.objects.all()
    serializer_class = AnnualReportRuSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['date',]

class QuarterReportRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = QuarterReportRu.objects.all()
    serializer_class = QuarterReportRuSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['year', 'quarter_type']


class PurposeRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PurposeRu.objects.all()
    serializer_class = PurposeRuSerializer


class PrincipleRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PrincipleRu.objects.all()
    serializer_class = PrincipleRuSerializer


class MediaRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MediaRu.objects.all()
    serializer_class = MediaRuSerializer


class SmiRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SmiRu.objects.all()
    serializer_class = SmiRuSerializer


class NewsRuImageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsRuImage.objects.all()
    serializer_class = NewsRuImageSerializer


class NormativeDocRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NormativeDocRu.objects.all()
    serializer_class = NormativeDocRuSerializer


class ContactInfoRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ContactInfoRu.objects.all()
    serializer_class = ContactInfoRuSerializer


class NewsVideoRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsVideoRu.objects.all()
    serializer_class = NewsVideoRuSerializer


class NewsRuPDFViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NewsRuPDF.objects.all()
    serializer_class = NewsRuPDFSerializer


class SmiVideoRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SmiVideoRu.objects.all()
    serializer_class = SmiVideoRuSerializer


class AnalyticRuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AnalyticRu.objects.all()
    serializer_class = AnalyticRuSerializer


@api_view(['GET'])
def search_all_models(request):
    q = request.GET.get('q')
    news = NewsRu.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    cases = CaseRu.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    an_reps = AnnualReportRu.objects.filter(Q(title__icontains=q))
    q_reps = QuarterReportRu.objects.filter(Q(title__icontains=q))
    docs = NormativeDocRu.objects.filter(Q(title__icontains=q))
    anals = AnalyticRu.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    faqs = FAQRu.objects.filter(Q(question__icontains=q) | Q(answer__icontains=q))
    team = TeamMemberRu.objects.filter(Q(full_name__icontains=q))
    data = {
        'news': NewsRuSerializer(news, many=True, allow_null=True).data,
        'cases': CaseRuSerializer(cases, many=True, allow_null=True).data,
        'annual_reports': AnnualReportRuSerializer(an_reps, many=True, allow_null=True).data,
        'quarter_reports': QuarterReportRuSerializer(q_reps, many=True, allow_null=True).data,
        'normative_docs': NormativeDocRuSerializer(docs, many=True, allow_null=True).data,
        'analytics': AnalyticRuSerializer(anals, many=True, allow_null=True).data,
        'faq': FAQRuSerializer(faqs, many=True, allow_null=True).data,
        'team_members': TeamMemberRuSerializer(team, many=True, allow_null=True).data
    }
    return Response(data)

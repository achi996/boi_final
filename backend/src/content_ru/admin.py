from django.contrib import admin

from common.admin import (
    CaseAdmin,
    AnnualReportAdmin,
    QuarterReportAdmin,
    FAQAdmin,
    TeamMemberAdmin
)
from .models import *


class NewsRuImageInline(admin.TabularInline):
    model = NewsRuImage
    extra = 0


class NewsVideoRuInline(admin.TabularInline):
    model = NewsVideoRu
    extra = 0


class NewsRuPDFInline(admin.TabularInline):
    model = NewsRuPDF
    extra = 0


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'date')
    inlines = [NewsRuImageInline, NewsVideoRuInline, NewsRuPDFInline]


class SmiVideoRuInline(admin.TabularInline):
    model = SmiVideoRu
    extra = 0


class SmiRuAdmin(admin.ModelAdmin):
    inlines = [SmiVideoRuInline]


admin.site.register(NewsRu, NewsAdmin)
admin.site.register(CaseRu, CaseAdmin)
admin.site.register(QuarterReportRu, QuarterReportAdmin)
admin.site.register(AnnualReportRu, AnnualReportAdmin)
admin.site.register(FAQRu, FAQAdmin)
admin.site.register(TeamMemberRu, TeamMemberAdmin)
admin.site.register(MissionRu)
admin.site.register(PurposeRu)
# admin.site.register(PrincipleRu)
admin.site.register(MediaRu)
admin.site.register(SmiRu, SmiRuAdmin)
admin.site.register(NormativeDocRu)
admin.site.register(ContactInfoRu)
admin.site.register(AnalyticRu)

from rest_framework import serializers

from content_ru.models import *


class FAQRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = FAQRu
        fields = '__all__'


class TeamMemberRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamMemberRu
        fields = '__all__'


class MissionRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = MissionRu
        fields = '__all__'


class NewsRuImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsRuImage
        fields = ('id', 'image')


class NewsRuPDFSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsRuPDF
        fields = ('id', 'file')


class NewsVideoRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsVideoRu
        fields = ('id', 'title', 'link')


class NewsRuSerializer(serializers.ModelSerializer):
    images = NewsRuImageSerializer(many=True, read_only=True)
    videos = NewsVideoRuSerializer(many=True, read_only=True)
    pdfs = NewsRuPDFSerializer(many=True, read_only=True)

    class Meta:
        model = NewsRu
        fields = '__all__'
        extra_fields = ['images', 'videos', 'pdfs']


class CaseRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaseRu
        fields = '__all__'


class AnnualReportRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnualReportRu
        fields = '__all__'

class QuarterReportRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuarterReportRu
        fields = '__all__'

class PurposeRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurposeRu
        fields = '__all__'


class PrincipleRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = PrincipleRu
        fields = '__all__'


class MediaRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaRu
        fields = '__all__'


class SmiRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmiRu
        fields = '__all__'


class NormativeDocRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = NormativeDocRu
        fields = '__all__'


class ContactInfoRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactInfoRu
        fields = '__all__'


class SmiVideoRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmiVideoRu
        fields = '__all__'


class AnalyticRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalyticRu
        fields = '__all__'

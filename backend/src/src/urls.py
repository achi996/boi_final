from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="EBRR API",
      default_version='v1',
      description="API FOR EBRR SITE",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="akmtchyngyz@gmail.com"),
      license=openapi.License(name="BSD License"),
   ),
   public=False,
   permission_classes=(permissions.IsAuthenticated,),
)


api_urlpatterns = [
    path('common/', include('common.urls')),
    path('kg/', include('content_kg.urls')),
    path('ru/', include('content_ru.urls')),
    path('en/', include('content_en.urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_urlpatterns)),
    path('api/swagger/',
         schema_view.with_ui('swagger', cache_timeout=0),
         name='schema-swagger-ui'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from rest_framework import viewsets, mixins

from common.serializers import *

from braces.views import CsrfExemptMixin


class ComplaintViewSet(CsrfExemptMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Complaint.objects.all()
    serializer_class = ComplaintSerializer
    authentication_classes = []


class ComplaintDocumentViewSet(CsrfExemptMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = ComplaintDocument.objects.all()
    serializer_class = ComplaintDocumentSerializer
    authentication_classes = []


class ConsultationViewSet(CsrfExemptMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Consultation.objects.all()
    serializer_class = ConsultationSerializer
    authentication_classes = []


class PartnerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer


class FirstSliderImageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FirstSliderImage.objects.all()
    serializer_class = FirstSliderImageSerializer


class SecondSliderImageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SecondSliderImage.objects.all()
    serializer_class = SecondSliderImageSerializer


class FeedbackViewSet(CsrfExemptMixin,
                      mixins.CreateModelMixin,
                      mixins.ListModelMixin,
                      viewsets.GenericViewSet):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    authentication_classes = []
from rest_framework.routers import DefaultRouter

from common import views


router = DefaultRouter()

router.register('complaints', views.ComplaintViewSet)
router.register('complaint-documents', views.ComplaintDocumentViewSet)
router.register('consultations', views.ConsultationViewSet)
router.register('feedback', views.FeedbackViewSet)
router.register('partners', views.PartnerViewSet)
router.register('first-sliders', views.FirstSliderImageViewSet)
router.register('second-sliders', views.SecondSliderImageViewSet)

urlpatterns = router.urls

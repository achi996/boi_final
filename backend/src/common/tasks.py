from django.core.mail import send_mail
from src.settings import EMAIL_HOST_USER, EMAILS_TO_SEND


def send_message(
        complaint_target,
        target_org_name,
        court_process,
        appeal,
        one_year,
        complaint_type,
        full_name_fiz,
        document_type,
        document_number,
        full_name_yur,
        head_of_organ,
        staff_name,
        iin_staff,
        reg_number_min_us,
        region,
        activity,
        other_activity,
        complaint_description,
        problem_solve,
        our_act,
        is_anonymous,
        reason_of_anonymity,
        applicant_name,
        address,
        email,
        response_type,
        created,
        need_money,
        money_amount,
        currency,
        ):
    if complaint_target == 'GE':
        ct = 'Предприятие госсектора'
    elif complaint_target == 'GO':
        ct = 'Государственный орган'
    elif complaint_target == 'LO':
        ct = 'Орган местного самоуправления'
    else:
        ct = 'Должностное лицо'
    ctype = 'Юр. лицо' if complaint_type == "B" else 'Физ. лицо'
    text = f"""
*) На кого Вы подаете жалобу? - {ct}
*) Укажите название организации или Ф.И.О., место работы и должность лица, на которое подается жалоба - {target_org_name}
*) Находится ли Ваша жалоба на стадии судебного или арбитражного разбирательства
или было ли по ней вынесено судебное или арбитражное решение? - {court_process}
*) Была ли использована возможность административного обжалования Вашей жалобы хотя бы в одной инстанции? - {appeal}
*) Прошёл ли один год с момента нарушения Ваших прав, свобод и законных интересов?  {one_year}
*) Вы подаете жалобу в качестве - {ctype},
*) ФИО физ лица - {full_name_fiz},
*) На основании какого документа Вы осуществляете свою деятельность - {document_type}
*) Серия/номер патента или номер свидетельства о государственной регистрации индивидуального предпринимателя - {document_number}
*) Полное официальное наименование юридического лица - {full_name_yur}
*) ФИО руководителя - {head_of_organ}
*) Ф.И.О. заявителя и занимаемая должность (если жалоба подается не руководителем) - {staff_name}
*) ИНН - {iin_staff}
*) Регистрационный номер Министерства юстиции КР - {reg_number_min_us}
*) Регион осуществления деятельности заявителя, где произошло нарушение прав, свобод и законных интересов заявителя - {region}
*) Сфера деятельности - {activity}
*) Другая сфера - {other_activity}
*) Cуть жалобы - {complaint_description}
*) Какие попытки были предприняты Вами самостоятельно для разрешения жалобы? - {problem_solve}
*) Что, по Вашему мнению, необходимо сделать Бизнес-омбудсмену, чтобы исправить ситуацию/решить проблему? - {our_act}
*) Анонимно - {is_anonymous}
*) Причина анонимности - {reason_of_anonymity}
*) Ф.И.О - {applicant_name}
*) Адрес - {address}
*) Электронная почта - {email}
*) Ответ в виде - {response_type}
*) Существует ли возможность измерить Вашу жалобу в денежном выражении? - {need_money}
*) Сумма - {money_amount} - {currency}
*) Создано - {created}
    """
    send_mail(
        "Новая жалоба с сайта boi.kg",
        text,
        EMAIL_HOST_USER,
        EMAILS_TO_SEND,
        fail_silently=True,
    )


def send_request(name, number, text):
    f = f"Имя: {name}\nТелефон: {number}\nКомментарии: {text}"
    send_mail(
        "Новая заявка на консультацию",
        f,
        EMAIL_HOST_USER,
        EMAILS_TO_SEND,
        fail_silently=True,
    )


def send_feedback(name, organization, text):
    f = f"Имя: {name}\nОрганизация: {organization}\nКомментарии: {text}"
    send_mail(
        "Новая обратная связь",
        f,
        EMAIL_HOST_USER,
        EMAILS_TO_SEND,
        fail_silently=True,
    )

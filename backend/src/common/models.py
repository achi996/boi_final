import os
import uuid

from django.db import models

from . import constants
from utils import generate_image_filename


def generate_document_filename(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = f'{str(uuid.uuid4())}.{ext}'
    return os.path.join('complaint_docs', new_filename)


class Complaint(models.Model):
    complaint_target = models.CharField(
        'На кого Вы подаете жалобу?',
        max_length=255,
        choices=constants.COMPLAINT_TARGET_CHOICES,
        null=True, blank=True
    )
    target_org_name = models.TextField(
        verbose_name="Название организации или Ф.И.О на которое подается жалоба",
        null=True, blank=True
    )
    court_process = models.CharField(
        "Жалоба на стадии судебного или арбитражного разбирательства",
        max_length=255,
        choices=constants.YES_OR_NO,
        null=True, blank=True
    )
    appeal = models.CharField(
        "Административное обжалование",
        max_length=255,
        choices=constants.YES_OR_NO,
        null=True, blank=True
    )
    one_year = models.CharField(
        "Один год с момента нарушения",
        max_length=255,
        choices=constants.YES_OR_NO,
        null=True, blank=True
    )
    complaint_type = models.CharField(
        "Жалоба в качестве",
        max_length=255,
        choices=constants.COMPLAINT_TYPE_CHOICES,
        null=True, blank=True
    )
    full_name_fiz = models.CharField(
        "ФИО физ лица",
        max_length=255,
        blank=True, null=True,
    )
    iin = models.CharField("ИНН", max_length=255, blank=True, null=True)
    document_type = models.CharField(
        'На основании документа',
        max_length=255,
        choices=constants.DOCUMENT_TYPE_CHOICES,
        blank=True, null=True
    )
    document_number = models.CharField(
        'Серия/номер',
        max_length=255,
        blank=True, null=True
    )
    full_name_yur = models.TextField(
        "Официальное наименование юр лица",
        blank=True, null=True
    )
    head_of_organ = models.CharField(
        'ФИО руководителя',
        max_length=255,
        blank=True, null=True,
    )
    staff_name = models.TextField(
        'ФИО заявителя и занимаемая должность',
        blank=True, null=True
    )
    iin_staff = models.CharField(
        "ИНН заявителя",
        max_length=255,
        blank=True,
        null=True
    )
    reg_number_min_us = models.CharField(
        "Регистрационный номер Министерства юстиции КР",
        max_length=255,
        blank=True, null=True
    )
    region = models.CharField(
        'Регион',
        max_length=255,
        choices=constants.REGIONS,
        null=True, blank=True
    )
    activity = models.CharField(
        'Сфера деятельности',
        max_length=255,
        choices=constants.ACTIVITY_FIELD,
        null=True, blank=True
    )
    other_activity = models.TextField(
        'Другая сфера деятельности',
        null=True, blank=True
    )
    complaint_description = models.TextField('Суть жалобы', null=True, blank=True)
    problem_solve = models.TextField("Попытки разрешения жалобы", null=True, blank=True)
    our_act = models.TextField("Необходимо сделать БО", null=True, blank=True)
    is_anonymous = models.CharField(
        "Анонимность",
        max_length=255,
        choices=constants.YES_OR_NO,
        null=True, blank=True
    )
    reason_of_anonymity = models.TextField('Причина анонимности', blank=True, null=True)
    applicant_name = models.CharField(
        'ФИО заявителя',
        max_length=255,
        null=True, blank=True
    )
    address = models.CharField('Адрес', max_length=255, null=True, blank=True)
    telephone_number = models.CharField(
        'Контактный телефон',
        max_length=255,
        null=True, blank=True
    )
    email = models.EmailField('Электронная почта', max_length=255, null=True, blank=True)
    response_type = models.CharField(
        'Cпособ ответа',
        max_length=255,
        choices=constants.RESPONSE_CHOICES,
        null=True, blank=True
    )
    created = models.DateTimeField(
        'Дата создания',
        auto_now_add=True,
    )
    need_money = models.CharField(
        "Возможность измерить жалобу в денежном выражении",
        max_length=255,
        choices=constants.YES_OR_NO,
        null=True, blank=True
    )
    money_amount = models.CharField('Сумма', max_length=255, null=True, blank=True)
    currency = models.CharField('Валюта', max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = 'Жалоба'
        verbose_name_plural = 'Жалобы'
        ordering = ('-id',)

    def __str__(self):
        return self.applicant_name


class ComplaintDocument(models.Model):
    document = models.FileField(upload_to=generate_document_filename)
    complaint = models.ForeignKey(Complaint, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.document.name

    class Meta:
        verbose_name = 'Документ жалобы'
        verbose_name_plural = 'Документы жалоб'


class Consultation(models.Model):
    full_name = models.CharField('ФИО', max_length=255)
    mobile = models.CharField('Контактный номер', max_length=255)
    question = models.TextField('Описание')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Заявка на консультацию'
        verbose_name_plural = 'Заявки на консультацию'
        ordering = ('-id',)

    def __str__(self):
        return self.full_name


class Partner(models.Model):
    logo = models.ImageField(upload_to=generate_image_filename)
    link = models.URLField(default='#')

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'
        ordering = ('-id',)


class FirstSliderImage(models.Model):
    image = models.ImageField(upload_to=generate_image_filename)

    class Meta:
        verbose_name = 'Изображение слайда на главной'
        verbose_name_plural = 'Изображения слайда на главной'
        ordering = ('-id',)


class SecondSliderImage(models.Model):
    image = models.ImageField(upload_to=generate_image_filename)

    class Meta:
        verbose_name = 'Изображение 2го слайда на главной'
        verbose_name_plural = 'Изображения 2го слайда на главной'
        ordering = ('-id',)


class Feedback(models.Model):
    full_name = models.CharField('ФИО', max_length=255)
    organization = models.CharField(
        'Организация', max_length=255, blank=True, null=True
    )
    text = models.TextField('Описание')
    date = models.DateTimeField('Дата и время', auto_now_add=True)

    def __str__(self):
        return self.full_name

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ('-id',)

from django.contrib import admin
from django.contrib.auth.models import Group
from import_export import resources
from import_export.admin import ExportActionModelAdmin

from .models import *


class ComplaintResource(resources.ModelResource):

    class Meta:
        model = Complaint

    def dehydrate_complaint_type(self, complaint):
        if complaint.complaint_type == 'B':
            return 'Юридического лица'
        return 'Физического лица'

    def dehydrate_complaint_target(self, complaint):
        if complaint.complaint_target == 'GO':
            return 'Государственный орган'
        elif complaint.complaint_target == 'LO':
            return 'Орган местного самоуправления'
        elif complaint.complaint_target == 'GE':
            return 'Предприятие госсектора'
        elif complaint.complaint_target == 'O':
            return 'Должностное лицо'


class CaseAdmin(admin.ModelAdmin):
    list_display = ('title', 'date')


class AnnualReportAdmin(admin.ModelAdmin):
    list_display = ('title', 'date')


class QuarterReportAdmin(admin.ModelAdmin):
    list_display = ('title', 'quarter_type', 'date')


class FAQAdmin(admin.ModelAdmin):
    list_display = ('question', 'answer')


class TeamMemberAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'position')


class ConsultationAdmin(admin.ModelAdmin):
    list_display = (
        'full_name', 'mobile',
        'question', 'created'
    )
    list_filter = ('created',)


class ComplaintDocInline(admin.TabularInline):
    model = ComplaintDocument
    extra = 0


class ComplaintAdmin(ExportActionModelAdmin):
    inlines = [ComplaintDocInline]
    list_display = (
        'target_org_name', 'complaint_target', 'complaint_type',
        'region', 'activity', 'is_anonymous', 'applicant_name',
        'address', 'telephone_number', 'email', 'response_type', 'created'
    )
    list_filter = (
        'complaint_target', 'complaint_type',
        'court_process', 'appeal', 'one_year',
        'response_type', 'created', 'is_anonymous',
        'activity', 'region',
    )
    resource_class = ComplaintResource


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'organization', 'date')
    readonly_fields = ('date',)


admin.site.register(Complaint, ComplaintAdmin)
admin.site.register(ComplaintDocument)
admin.site.register(Consultation, ConsultationAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Partner)
admin.site.register(FirstSliderImage)
admin.site.register(SecondSliderImage)

admin.site.unregister(Group)

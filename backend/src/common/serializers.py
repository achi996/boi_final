from rest_framework import serializers
from common.models import *
from common.tasks import send_message, send_request, send_feedback


class ComplaintSerializer(serializers.ModelSerializer):
    class Meta:
        model = Complaint
        fields = '__all__'

    def create(self, validated_data):
        complaint_target = validated_data.get('complaint_target', None)
        target_org_name = validated_data.get('target_org_name', None)
        court_process = validated_data.get('court_process', None)
        appeal = validated_data.get('appeal', None)
        one_year = validated_data.get('one_year', None)
        complaint_type = validated_data.get('complaint_type', None)
        full_name_fiz = validated_data.get('full_name_fiz', None)
        document_type = validated_data.get('document_type', None)
        document_number = validated_data.get('document_number', None)
        full_name_yur = validated_data.get('full_name_yur', None)
        head_of_organ = validated_data.get('head_of_organ', None)
        staff_name = validated_data.get('staff_name', None)
        iin_staff = validated_data.get('iin_staff', None)
        reg_number_min_us = validated_data.get('reg_number_min_us', None)
        region = validated_data.get('region', None)
        activity = validated_data.get('activity', None)
        other_activity = validated_data.get('other_activity', None)
        complaint_description = validated_data.get('complaint_description', None)
        problem_solve = validated_data.get('problem_solve', None)
        our_act = validated_data.get('our_act', None)
        is_anonymous = validated_data.get('is_anonymous', None)
        reason_of_anonymity = validated_data.get('reason_of_anonymity', None)
        applicant_name = validated_data.get('applicant_name', None)
        address = validated_data.get('address', None)
        email = validated_data.get('email', None)
        response_type = validated_data.get('response_type', None)
        created = validated_data.get('created', None)
        need_money = validated_data.get('need_money', None)
        money_amount = validated_data.get('money_amount', None)
        currency = validated_data.get('currency', None)
        send_message(
            complaint_target,
            target_org_name,
            court_process,
            appeal,
            one_year,
            complaint_type,
            full_name_fiz,
            document_type,
            document_number,
            full_name_yur,
            head_of_organ,
            staff_name,
            iin_staff,
            reg_number_min_us,
            region,
            activity,
            other_activity,
            complaint_description,
            problem_solve,
            our_act,
            is_anonymous,
            reason_of_anonymity,
            applicant_name,
            address,
            email,
            response_type,
            created,
            need_money,
            money_amount,
            currency,
        )
        return super().create(validated_data)


class ComplaintDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComplaintDocument
        fields = '__all__'


class ConsultationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consultation
        fields = '__all__'

    def create(self, validated_data):
        name = validated_data.get('full_name')
        mobile = validated_data.get('mobile')
        question = validated_data.get('question')
        send_request(name, mobile, question)
        return super().create(validated_data)


class PartnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = '__all__'


class FirstSliderImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FirstSliderImage
        fields = '__all__'


class SecondSliderImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = SecondSliderImage
        fields = '__all__'


class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = '__all__'

    def create(self, validated_data):
        name = validated_data.get('full_name')
        organization = validated_data.get('organization')
        text = validated_data.get('text')
        send_feedback(name, organization, text)
        return super().create(validated_data)

COMPLAINT_TARGET_CHOICES = (
    ('GO', 'Государственный орган'),
    ('LO', 'Орган местного самоуправления'),
    ('GE', 'Предприятие госсектора'),
    ('O', 'Должностное лицо')
)


YES_OR_NO = (
    ('YES', 'Да'),
    ('NO', 'Нет')
)


COMPLAINT_TYPE_CHOICES = (
    ('B', 'Юридического лица'),
    ('NP', 'Физического лица'),
)


REGIONS = (
    ('BISHKEK', 'Бишкек'),
    ('CHUY', 'Чуйская область'),
    ('OSH', 'Ош'),
    ('OSH_REGION', 'Ошская область'),
    ('YSSYK_KUL', 'Иссык-Кульская область'),
    ('TALAS', 'Таласская область'),
    ('NARYN', 'Нарынская область'),
    ('DJALALABAD', 'Джалал-Абадская область'),
    ('BATKEN', 'Баткенская область'),
)

ACTIVITY_FIELD = (
    ("MINING_INDUSTRY", 'Горнодобывающая отрасль'),
    ("IT_SECTOR", 'ИТ-сектор'),
    ("TELECOMMUNICATION", 'Телекоммуникации'),
    ("SERVICE_FIELD", 'Сфера услуг'),
    ("LIGHT_INDUSTRY", 'Легкая промышленность'),
    ("MEDICINE", 'Медицина'),
    ("EDUCATION", 'Образование'),
    ("MANUFACTURE", 'Производство'),
    ("RECYCLE_AGRICULTURE", "Переработка сельскохозяйственной продукции"),
    ("AGRICULTURE", 'Сельское хозяйство'),
    ("BUILDING", 'Строительство'),
    ("TRADE", 'Торговля'),
    ("TOURISM", "Туризм"),
    ("TRANSPORT", 'Транспорт'),
    ("FINANCIAL_SERVICE", 'Финансовые услуги'),
    ("OTHER", 'Другое'),
)

RESPONSE_CHOICES = (
    ("EMAIL", "Электронная почта"),
    ('MAIL', 'Почта')
)


DOCUMENT_TYPE_CHOICES = (
    ('PATENT', 'Патент'),
    ('CERTIFICATE', 'Свидетельство о гос регистрации ИП')
)

#!/bin/sh

until cd /app/backend/src
do
    echo "Waiting for server volume..."
done
./manage.py migrate

./manage.py collectstatic --noinput

gunicorn src.wsgi --bind 0.0.0.0:8000 --workers 2 --threads 2
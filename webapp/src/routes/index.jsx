import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import Home from '../views/Home';
import NormativeDocuments from '../views/AboutUs/NormativeDocuments';
import NormativeBase from '../views/AboutUs/NormativeBase';
import OurTeam from '../views/AboutUs/OurTeam';
import FAQs from '../views/AboutUs/FAQs';
import ApplyComplaint from '../views/Complaint/ApplyComplaint';
import Eligibility from '../views/Complaint/Eligibility/';
import Consultation from '../views/Consultation';
import MassMedia from '../views/MassMedia';
import MassMediaDetail from '../views/MassMediaDetail';
import MediaMaterials from '../views/MediaMaterials';
import MediaMaterialDetail from '../views/MediaMaterialDetail';
import ContactsPage from '../views/ContactsPage/ContactsPage';
import NotFound from '../views/NotFound';
import NewsListPage from '../views/NewsListPage';
import AnalyticsListPage from '../views/AnalyticsListPage';
import NewsDetailPage from '../views/NewsDetailPage';
import CasesPage from '../views/CasesPage/CasesPage';
import ReviewPage from '../views/ReviewPage';
import ReviewsPage from '../views/ReviewsPage';
import CaseDetalPage from '../views/CaseDetail';
import AnnualReports from '../views/ReportsPage/AnnualReports';
import QuarterReports from '../views/ReportsPage/QuarterReports';
import AnalyticsDetailPage from '../views/AnalyticsDetailPage';
import history from '../_helpers/history';

const Routes = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path={`/`} component={Home} exact />
        <Route path={`/faq`} component={FAQs} exact />
        <Route path={`/eligibility`} component={Eligibility} exact />
        <Route path={`/consultation`} component={Consultation} exact />
        <Route path={`/review`} component={ReviewPage} exact />
        <Route path={`/reviews`} component={ReviewsPage} exact />
        <Route path={`/complaint`} component={ApplyComplaint} exact />
        <Route path={`/annual-reports`} component={AnnualReports} exact />
        <Route path={`/quarter-reports`} component={QuarterReports} exact />
        <Route path={`/cases`} component={CasesPage} exact />
        <Route path={`/cases/:id`} component={CaseDetalPage} exact />
        <Route path={`/normative-base`} component={NormativeBase} exact />
        <Route
          path={`/normative-documents`}
          component={NormativeDocuments}
          exact
        />
        <Route path={`/our-team`} component={OurTeam} exact />
        <Route path={`/contacts`} component={ContactsPage} exact />
        <Route path={`/mass-media`} component={MassMedia} exact />
        <Route path={`/mass-media/:id`} component={MassMediaDetail} exact />
        <Route path={`/media-materials`} component={MediaMaterials} exact />
        <Route
          path={`/media-materials/:id`}
          component={MediaMaterialDetail}
          exact
        />
        <Route path={`/:lang(ru|kg|en)/news`} component={NewsListPage} exact />
        <Route
          path={`/:lang(ru|kg|en)/analytics`}
          component={AnalyticsListPage}
          exact
        />
        <Route
          path={`/:lang(ru|kg|en)/news/:id`}
          component={NewsDetailPage}
          exact
        />
        <Route
          path={`/:lang(ru|kg|en)/analytics/:id`}
          component={AnalyticsDetailPage}
          exact
        />
        <Route path={`/notfound`} component={NotFound} exact />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
};

export default Routes;

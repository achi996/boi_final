import { createContext, useState, useEffect } from 'react';
import Routes from './routes';
import './App.css';

export const LanguageContext = createContext(null);

const splited_path = window.location.pathname.split('/');

function App() {
  const [lang, setLang] = useState(
    ['kg', 'en', 'ru'].includes(splited_path[1]) ? splited_path[1] : 'ru'
  );

  useEffect(() => {
    if (splited_path[1] === 'ru' && lang !== 'ru') {
      setLang('ru');
    } else if (splited_path[1] === 'en' && lang !== 'en') {
      setLang('en');
    } else if (splited_path[1] === 'kg' && lang !== 'kg') {
      setLang('kg');
    }
  }, [lang]);

  return (
    <LanguageContext.Provider
      value={{
        lang,
        setLang,
      }}
    >
      <Routes />
    </LanguageContext.Provider>
  );
}

export default App;

import React from 'react';
import clsx from 'clsx';
import { useTheme } from '@material-ui/core';
import Footer from './Footer';
import Nav from './Nav';
import { useStyles } from './muiStyles';

export default function BaseLayout(props) {
  const theme = useTheme();
  const classes = useStyles();
  const [openSidebar, setOpenSidebar] = React.useState(false);

  const handleSidebarOpen = () => {
    setOpenSidebar(true);
  };

  const handleSidebarClose = () => {
    setOpenSidebar(false);
  };

  return (
    <>
      <Nav
        sidebarClasses={classes}
        theme={theme}
        handleSidebarOpen={handleSidebarOpen}
        handleSidebarClose={handleSidebarClose}
        openSidebar={openSidebar}
      />
      <main
        className={clsx({
          [classes.contentShift]: openSidebar,
        })}
      >
        {props.children}
      </main>
      <Footer />
    </>
  );
}

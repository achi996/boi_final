import React from 'react';

export default function NormativeDoc({ title, url }) {
  return (
    <div class="single-document">
      <p>{title}</p>
      <a href={url} target="_blank" rel="noreferrer">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="99"
          height="110"
          viewBox="0 0 99 110"
        >
          <g
            id="Group_119"
            data-name="Group 119"
            transform="translate(-612 -165)"
          >
            <rect
              id="Rectangle_98"
              data-name="Rectangle 98"
              width="99"
              height="110"
              transform="translate(612 165)"
              fill="#2653ad"
            />
            <g
              id="Group_15"
              data-name="Group 15"
              transform="translate(-1466.55 -16)"
            >
              <path
                id="Path_2"
                data-name="Path 2"
                d="M2144.55,241.561v8a4,4,0,0,1-4,4h-28a4,4,0,0,1-4-4v-8"
                fill="none"
                stroke="#fff"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="3"
              />
              <path
                id="Path_3"
                data-name="Path 3"
                d="M2116.55,231.561l10,10,10-10"
                fill="none"
                stroke="#fff"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="3"
              />
              <line
                id="Line_5"
                data-name="Line 5"
                y1="24"
                transform="translate(2126.55 217.561)"
                fill="none"
                stroke="#fff"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="3"
              />
            </g>
          </g>
        </svg>
      </a>
    </div>
  );
}

import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import Truncate from 'react-truncate';
import { Button } from '@material-ui/core';
import { formatDate } from '../../_helpers/date';

const AnalyticsCard = ({ analytics, langState }) => {
  const wasDescriptionTruncatedInitially = analytics.description.length > 350;

  const [isDescriptionTruncated, setIsDescriptionTruncated] = useState(
    analytics.description.length > 350
  );

  const handleShowFullDescription = () => {
    setIsDescriptionTruncated(false);
  };

  const handleShowShortDescription = () => {
    setIsDescriptionTruncated(true);
  };

  return (
    <div className="single-new">
      <p className="single-new-date standart_p">
        {formatDate(analytics.date.split('-'))}
      </p>
      <p className="standart_p single-new-title">
        <NavLink to={`/${langState.lang}/analytics/${analytics.id}`}>
          {analytics.title}
        </NavLink>
      </p>

      {isDescriptionTruncated ? (
        <Truncate
          className="standart_p single-new-text"
          style={{ marginTop: 10 }}
          lines={2}
          ellipsis={<span>...</span>}
        >
          {ReactHtmlParser(analytics.description)}
        </Truncate>
      ) : (
        <p className="standart_p single-new-text" style={{ marginTop: 10 }}>
          {ReactHtmlParser(analytics.description)}
        </p>
      )}

      {isDescriptionTruncated ? (
        <Button
          variant="contained"
          color="primary"
          className="next_button"
          onClick={handleShowFullDescription}
        >
          {langState.lang === 'ru'
            ? 'Далее'
            : langState.lang === 'kg'
            ? 'Дагы'
            : 'More'}
        </Button>
      ) : wasDescriptionTruncatedInitially ? (
        <Button
          variant="contained"
          color="primary"
          className="next_button"
          onClick={handleShowShortDescription}
        >
          {langState.lang === 'ru'
            ? 'Назад'
            : langState.lang === 'kg'
            ? 'Артка'
            : 'Back'}
        </Button>
      ) : null}
    </div>
  );
};

export default AnalyticsCard;

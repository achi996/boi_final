import React, { useState } from 'react';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import SearchModal from './SearchModal';

const Search = () => {
  const [isSearchModalOpen, setIsSearchModalOpen] = useState(false);

  const handleOpenSearchModal = () => {
    setIsSearchModalOpen(true);
  };

  const handleCloseSearchModal = () => {
    setIsSearchModalOpen(false);
  };

  return (
    <>
      <IconButton color='primary' onClick={handleOpenSearchModal}>
        <SearchIcon />
      </IconButton>
      <SearchModal
        isOpen={isSearchModalOpen}
        handleClose={handleCloseSearchModal}
      />
    </>
  );
};

export default Search;

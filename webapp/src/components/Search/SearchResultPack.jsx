import React, { useContext } from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { LanguageContext } from '../../App';
import { truncateText } from '../../_helpers/truncateText';
import classes from './Search.module.css';

const SearchResultPack = ({
  resultPackRuHeader,
  resultPackKgHeader,
  resultPackEnHeader,
  searchResults,
  limit,
  handleResultClick,
  handleIncreaseResultsToShowLimit,
}) => {
  const langState = useContext(LanguageContext);

  return (
    <>
      {searchResults?.length ? (
        <div>
          <Typography className={classes.seperatorHeader}>
            {langState.lang === 'ru'
              ? resultPackRuHeader
              : langState.lang === 'kg'
              ? resultPackKgHeader
              : resultPackEnHeader}
          </Typography>
          <div>
            {searchResults.slice(0, limit).map((result) => (
              <Typography
                key={`${result.id}`}
                className={classes.searchItem}
                onClick={handleResultClick(result.id)}
              >
                {truncateText(result.title, 75)}
              </Typography>
            ))}

            {searchResults?.length - limit > 0 ? (
              <Button
                className={classes.moreButton}
                variant={'contained'}
                onClick={handleIncreaseResultsToShowLimit}
              >
                {langState.lang === 'ru'
                  ? 'Еще'
                  : langState.lang === 'kg'
                  ? 'Дагы'
                  : 'More'}
              </Button>
            ) : null}
          </div>
        </div>
      ) : null}
    </>
  );
};

export default SearchResultPack;

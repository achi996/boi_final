import React, { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import Modal from '@material-ui/core/Modal';
import Collapse from '@material-ui/core/Collapse';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import CloseIcon from '@material-ui/icons/Close';
import { IconButton } from '@material-ui/core';
import Loader from '../UI/Loader/Loader';
import { LanguageContext } from '../../App';
import history from '../../_helpers/history';
import classes from './Search.module.css';
import SearchResultPack from './SearchResultPack';

const { REACT_APP_API_URL } = process.env;

const fetchSearchResults = async (value, locale) => {
  if (value === '') {
    return [];
  } else {
    try {
      const { data } = await axios.get(
        `${REACT_APP_API_URL}/${locale}/search/?q=${value}`
      );
      return data;
    } catch (e) {
      console.log(e.response.data);
      return [];
    }
  }
};

const DEFAUL_RESULTS_LIMIT = {
  news: 10,
  cases: 10,
  annual_reports: 10,
  quarter_reports: 10,
  normative_docs: 10,
  analytics: 10,
  faq: 10,
  team_members: 10,
};

const SearchModal = ({ isOpen, handleClose }) => {
  const [searchValue, setSearchValue] = useState('');
  const [searchResults, setSearchResults] = useState(null);
  const [searchResultsAmount, setSearchResultsAmount] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const langState = useContext(LanguageContext);

  const [resultsToShowLimit, setResultsToShowLimit] =
    useState(DEFAUL_RESULTS_LIMIT);

  const handleSearchValueChange = (e) => {
    setSearchValue(e.target.value);
  };

  const handleSearchItemClick = (resultType, id) => () => {
    if (resultType === 'news') {
      history.push(`/${langState.lang}/news/${id}`);
      document.location.reload();
    } else if (resultType === 'analytics') {
      history.push(`/${langState.lang}/analytics/${id}`);
      document.location.reload();
    } else if (resultType === 'faq') {
      history.push(`/faq/`);
    } else if (resultType === 'annual_reports') {
      history.push(`/annual-reports/`);
    } else if (resultType === 'quarter_reports') {
      history.push(`/quarter-reports/`);
    } else if (resultType === 'normative_docs') {
      history.push(`/normative-documents/`);
    } else if (resultType === 'team_members') {
      history.push(`/our-team/`);
    } else {
      history.push(`/cases/${id}`);
      document.location.reload();
    }
  };

  const handleIncreaseResultsToShowLimit = (resultType) => {
    setResultsToShowLimit({
      ...resultsToShowLimit,
      [resultType]: resultsToShowLimit[resultType] + 10,
    });
  };

  const searchLabel =
    langState.lang === 'ru'
      ? 'Поиск'
      : langState.lang === 'kg'
      ? 'Издөө'
      : 'Search';

  useEffect(() => {
    if (searchValue) {
      setIsLoading(true);
      fetchSearchResults(searchValue, langState.lang).then((results) => {
        setSearchResults(results);
        const { news, cases, analytics } = results;
        let searchResultsAmount = news.length + cases.length + analytics.length;
        setSearchResultsAmount(searchResultsAmount);
        setIsLoading(false);
      });
    } else {
      setSearchResults(null);
      setResultsToShowLimit(DEFAUL_RESULTS_LIMIT);
      setSearchResultsAmount(0);
    }
  }, [langState.lang, searchValue]);

  const handleCloseAndResetState = () => {
    setSearchValue('');
    setSearchResults(null);
    setResultsToShowLimit(DEFAUL_RESULTS_LIMIT);
    setSearchResultsAmount(0);
    handleClose();
  };

  return (
    <Modal
      className={classes.searchModal}
      open={isOpen}
      onClose={handleCloseAndResetState}
    >
      <div className={classes.formWrapper}>
        {isLoading ? <Loader /> : null}
        <form>
          <IconButton
            className={classes.closeIcon}
            onClick={handleCloseAndResetState}
          >
            <CloseIcon />
          </IconButton>
          <TextField
            value={searchValue}
            onChange={handleSearchValueChange}
            label={searchLabel}
            className={classes.input}
            variant={'filled'}
            color={'primary'}
            labelWidth={searchLabel.length * 9}
            placeholder={searchLabel}
            autoFocus={true}
          />

          <Collapse in={searchResultsAmount}>
            <Paper className={classes.searchList}>
              <SearchResultPack
                resultPackRuHeader={'Новости'}
                resultPackKgHeader={'Жаңылыктар'}
                resultPackEnHeader={'News'}
                searchResults={searchResults?.news}
                limit={resultsToShowLimit.news}
                handleResultClick={(resultId) =>
                  handleSearchItemClick('news', resultId)
                }
                handleIncreaseResultsToShowLimit={() =>
                  handleIncreaseResultsToShowLimit('news')
                }
              />

              <SearchResultPack
                resultPackRuHeader={'Часто Задаваемые Вопросы'}
                resultPackKgHeader={'Суроо-жооптор'}
                resultPackEnHeader={'FAQ'}
                searchResults={
                  searchResults?.faq
                    ? searchResults?.faq.map((faqResult) => ({
                        id: faqResult.id,
                        title: faqResult.question,
                      }))
                    : []
                }
                limit={resultsToShowLimit.faq}
                handleResultClick={() => handleSearchItemClick('faq')}
                handleIncreaseResultsToShowLimit={() =>
                  handleIncreaseResultsToShowLimit('faq')
                }
              />

              <SearchResultPack
                resultPackRuHeader={'Нормативные документы'}
                resultPackKgHeader={'Нормативдик документтер'}
                resultPackEnHeader={'Regulations'}
                searchResults={searchResults?.normative_docs}
                limit={resultsToShowLimit.normative_docs}
                handleResultClick={() =>
                  handleSearchItemClick('normative_docs')
                }
                handleIncreaseResultsToShowLimit={() =>
                  handleIncreaseResultsToShowLimit('normative_docs')
                }
              />

              <SearchResultPack
                resultPackRuHeader={'Годовые отчеты'}
                resultPackKgHeader={'Жылдык отчёттор'}
                resultPackEnHeader={'Annual reports'}
                searchResults={searchResults?.annual_reports}
                limit={resultsToShowLimit.annual_reports}
                handleResultClick={() =>
                  handleSearchItemClick('annual_reports')
                }
                handleIncreaseResultsToShowLimit={() =>
                  handleIncreaseResultsToShowLimit('annual_reports')
                }
              />

              <SearchResultPack
                resultPackRuHeader={'Квартальные отчеты'}
                resultPackKgHeader={'Кварталдык отчёттор'}
                resultPackEnHeader={'Quarter reports'}
                searchResults={searchResults?.quarter_reports}
                limit={resultsToShowLimit.quarter_reports}
                handleResultClick={() =>
                  handleSearchItemClick('quarter_reports')
                }
                handleIncreaseResultsToShowLimit={() =>
                  handleIncreaseResultsToShowLimit('quarter_reports')
                }
              />

              <SearchResultPack
                resultPackRuHeader={'Аналитика'}
                resultPackKgHeader={'Аналитика'}
                resultPackEnHeader={'Analytics'}
                searchResults={searchResults?.analytics}
                limit={resultsToShowLimit.analytics}
                handleResultClick={(resultId) =>
                  handleSearchItemClick('analytics', resultId)
                }
                handleIncreaseResultsToShowLimit={() =>
                  handleIncreaseResultsToShowLimit('analytics')
                }
              />

              <SearchResultPack
                resultPackRuHeader={'Команда'}
                resultPackKgHeader={'Кызматкерлерибиз'}
                resultPackEnHeader={'Team'}
                searchResults={
                  searchResults?.team_members
                    ? searchResults?.team_members.map((team_member) => ({
                        id: team_member.id,
                        title: `${team_member.full_name} - ${team_member.position}`,
                      }))
                    : []
                }
                limit={resultsToShowLimit.team_members}
                handleResultClick={() => handleSearchItemClick('team_members')}
                handleIncreaseResultsToShowLimit={() =>
                  handleIncreaseResultsToShowLimit('team_members')
                }
              />

              <SearchResultPack
                resultPackRuHeader={'Кейсы'}
                resultPackKgHeader={'Кейстер'}
                resultPackEnHeader={'Cases'}
                searchResults={searchResults?.cases}
                limit={resultsToShowLimit.cases}
                handleResultClick={(resultId) =>
                  handleSearchItemClick('cases', resultId)
                }
                handleIncreaseResultsToShowLimit={() =>
                  handleIncreaseResultsToShowLimit('cases')
                }
              />
            </Paper>
          </Collapse>
        </form>
      </div>
    </Modal>
  );
};

export default SearchModal;

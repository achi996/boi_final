export const complaint_targets = [
  {
    id: 0,
    label: 'Государственный орган',
    value: 'GO',
  },
  {
    id: 1,
    label: 'Орган местного самоуправления',
    value: 'LO',
  },
  {
    id: 2,
    label: 'Предприятие государственного сектора',
    value: 'GE',
  },
  {
    id: 3,
    label: 'Должностное лицо',
    value: 'O',
  },
];

export const complaint_targets_kg = [
  {
    id: 0,
    label: 'Мамлекеттик орган',
    value: 'GO',
  },
  {
    id: 1,
    label: 'Жергиликтүү ѳз алдынча башкаруу органы',
    value: 'LO',
  },
  {
    id: 2,
    label: 'Мамлекеттик сектордогу ишкана',
    value: 'GE',
  },
  {
    id: 3,
    label: 'Кызмат адамы',
    value: 'O',
  },
];

export const complaint_targets_en = [
  {
    id: 0,
    label: 'State authority',
    value: 'GO',
  },
  {
    id: 1,
    label: 'Municipal authority',
    value: 'LO',
  },
  {
    id: 2,
    label: 'State owned enterprise',
    value: 'GE',
  },
  {
    id: 3,
    label: 'Civil servant or official',
    value: 'O',
  },
];

export const confirm_options = [
  {
    id: 0,
    label: 'Да',
    value: 'YES',
  },
  {
    id: 1,
    label: 'Нет',
    value: 'NO',
  },
];

export const confirm_options_kg = [
  {
    id: 0,
    label: 'Ооба',
    value: 'YES',
  },
  {
    id: 1,
    label: 'Жок',
    value: 'NO',
  },
];

export const confirm_options_en = [
  {
    id: 0,
    label: 'Yes',
    value: 'YES',
  },
  {
    id: 1,
    label: 'No',
    value: 'NO',
  },
];

export const complaint_types = [
  {
    id: 0,
    label: 'Юридического лица',
    value: 'B',
  },
  {
    id: 1,
    label: 'Физического лица',
    value: 'NP',
  },
];

export const complaint_types_kg = [
  {
    id: 0,
    label: 'Юридикалык жак',
    value: 'B',
  },
  {
    id: 1,
    label: 'Физикалык жак',
    value: 'NP',
  },
];

export const complaint_types_en = [
  {
    id: 0,
    label: 'Legal entity',
    value: 'B',
  },
  {
    id: 1,
    label: 'Individual entrepreneur',
    value: 'NP',
  },
];

export const document_types = [
  {
    id: 0,
    label: 'Патент',
    value: 'PATENT',
  },
  {
    id: 1,
    label:
      'Свидетельство о государственной регистрации индивидуального предпринимателя',
    value: 'CERTIFICATE',
  },
];

export const document_types_kg = [
  {
    id: 0,
    label: 'Патент',
    value: 'PATENT',
  },
  {
    id: 1,
    label: 'Жеке ишкердин мамлекеттик каттоосу жөнүндө күбөлүгү',
    value: 'CERTIFICATE',
  },
];

export const document_types_en = [
  {
    id: 0,
    label: 'Patent',
    value: 'PATENT',
  },
  {
    id: 1,
    label:
      'Certificate of the state registration of an individual entrepreneur',
    value: 'CERTIFICATE',
  },
];

export const regions = [
  {
    id: 0,
    label: 'г. Бишкек',
    value: 'BISHKEK',
  },
  {
    id: 1,
    label: 'Чуйская область',
    value: 'CHUY',
  },
  {
    id: 2,
    label: 'г. Ош',
    value: 'OSH',
  },
  {
    id: 3,
    label: 'Ошская область',
    value: 'OSH_REGION',
  },
  {
    id: 4,
    label: 'Иссык-Кульская область',
    value: 'YSSYK_KUL',
  },
  {
    id: 5,
    label: 'Таласская область',
    value: 'TALAS',
  },
  {
    id: 6,
    label: 'Нарынская область',
    value: 'NARYN',
  },
  {
    id: 7,
    label: 'Джалал-Абадская область',
    value: 'DJALALABAD',
  },
  {
    id: 8,
    label: 'Баткенская область',
    value: 'BATKEN',
  },
];

export const regions_kg = [
  {
    id: 0,
    label: 'Бишкек шаары',
    value: 'BISHKEK',
  },
  {
    id: 1,
    label: 'Чүй облусу',
    value: 'CHUY',
  },
  {
    id: 2,
    label: 'Ош шаары',
    value: 'OSH',
  },
  {
    id: 3,
    label: 'Ош облусу',
    value: 'OSH_REGION',
  },
  {
    id: 4,
    label: 'Ысык-Көл облусу',
    value: 'YSSYK_KUL',
  },
  {
    id: 5,
    label: 'Талас облусу',
    value: 'TALAS',
  },
  {
    id: 6,
    label: 'Нарын облусу',
    value: 'NARYN',
  },
  {
    id: 7,
    label: 'Жалал-Абад облусуь',
    value: 'DJALALABAD',
  },
  {
    id: 8,
    label: 'Баткен облусу',
    value: 'BATKEN',
  },
];

export const regions_en = [
  {
    id: 0,
    label: 'Bishkek city',
    value: 'BISHKEK',
  },
  {
    id: 1,
    label: 'Chui region',
    value: 'CHUY',
  },
  {
    id: 2,
    label: 'Osh city',
    value: 'OSH',
  },
  {
    id: 3,
    label: 'Osh region',
    value: 'OSH_REGION',
  },
  {
    id: 4,
    label: 'Issyk-Kul region',
    value: 'YSSYK_KUL',
  },
  {
    id: 5,
    label: 'Talas region',
    value: 'TALAS',
  },
  {
    id: 6,
    label: 'Naryn region',
    value: 'NARYN',
  },
  {
    id: 7,
    label: 'Jalal-Abad region',
    value: 'DJALALABAD',
  },
  {
    id: 8,
    label: 'Batken region',
    value: 'BATKEN',
  },
];

export const activities = [
  {
    id: 0,
    label: 'Легкая промышленность',
    value: 'LIGHT_INDUSTRY',
  },
  {
    id: 1,
    label: 'Горнодобывающая отрасль',
    value: 'MINING_INDUSTRY',
  },
  {
    id: 2,
    label: 'ИТ-сектор',
    value: 'IT_SECTOR',
  },
  {
    id: 13,
    label: 'Телекоммуникации',
    value: 'TELECOMMUNICATION',
  },
  {
    id: 3,
    label: 'Сфера услуг',
    value: 'SERVICE_FIELD',
  },
  {
    id: 4,
    label: 'Транспорт',
    value: 'TRANSPORT',
  },
  {
    id: 5,
    label: 'Производство',
    value: 'MANUFACTURE',
  },
  {
    id: 6,
    label: 'Переработка сельскохозяйственной продукции',
    value: 'RECYCLE_AGRICULTURE',
  },
  {
    id: 7,
    label: 'Сельское хозяйство',
    value: 'AGRICULTURE',
  },
  {
    id: 8,
    label: 'Финансовые услуги',
    value: 'FINANCIAL_SERVICE',
  },
  {
    id: 9,
    label: 'Торговля',
    value: 'TRADE',
  },
  {
    id: 10,
    label: 'Строительство',
    value: 'BUILDING',
  },
  {
    id: 11,
    label: 'Туризм',
    value: 'TOURISM',
  },
  {
    id: 12,
    label: 'Другое',
    value: 'OTHER',
  },
];

export const activities_en = [
  {
    id: 0,
    label: 'Light industry',
    value: 'LIGHT_INDUSTRY',
  },
  {
    id: 1,
    label: 'Mining',
    value: 'MINING_INDUSTRY',
  },
  {
    id: 2,
    label: 'IT',
    value: 'IT_SECTOR',
  },
  {
    id: 3,
    label: 'Services',
    value: 'SERVICE_FIELD',
  },
  {
    id: 4,
    label: 'Transportation',
    value: 'TRANSPORT',
  },
  {
    id: 13,
    label: 'Telecommunication',
    value: 'TELECOMMUNICATION',
  },
  {
    id: 5,
    label: 'Manufacturing',
    value: 'MANUFACTURE',
  },
  {
    id: 6,
    label: 'Processing of agriculture produces',
    value: 'RECYCLE_AGRICULTURE',
  },
  {
    id: 7,
    label: 'Agriculture',
    value: 'AGRICULTURE',
  },
  {
    id: 8,
    label: 'Finances',
    value: 'FINANCIAL_SERVICE',
  },
  {
    id: 9,
    label: 'Retail',
    value: 'TRADE',
  },
  {
    id: 10,
    label: 'Construction',
    value: 'BUILDING',
  },
  {
    id: 11,
    label: 'Tourism',
    value: 'TOURISM',
  },
  {
    id: 12,
    label: 'Other',
    value: 'OTHER',
  },
];

export const activities_kg = [
  {
    id: 0,
    label: 'Жеңил өнөр-жай',
    value: 'LIGHT_INDUSTRY',
  },
  {
    id: 1,
    label: 'Тоо-кен өнөр-жайы',
    value: 'MINING_INDUSTRY',
  },
  {
    id: 2,
    label: 'Маалымат жана байланыш технологиялары',
    value: 'IT_SECTOR',
  },
  {
    id: 13,
    label: 'Телекоммуникация',
    value: 'TELECOMMUNICATION',
  },
  {
    id: 3,
    label: 'Тейлөө чөйрөсү',
    value: 'SERVICE_FIELD',
  },
  {
    id: 4,
    label: 'Транспорт',
    value: 'TRANSPORT',
  },
  {
    id: 5,
    label: 'Өндүрүш',
    value: 'MANUFACTURE',
  },
  {
    id: 6,
    label: 'Айыл чарба продукциясын кайра иштетүү',
    value: 'RECYCLE_AGRICULTURE',
  },
  {
    id: 7,
    label: 'Айыл чарба',
    value: 'AGRICULTURE',
  },
  {
    id: 8,
    label: 'Финансы өндүрүш',
    value: 'FINANCIAL_SERVICE',
  },
  {
    id: 9,
    label: 'Соода',
    value: 'TRADE',
  },
  {
    id: 10,
    label: 'Курулуш',
    value: 'BUILDING',
  },
  {
    id: 11,
    label: 'Туризм',
    value: 'TOURISM',
  },
  {
    id: 12,
    label: 'Башка',
    value: 'OTHER',
  },
];

export const response_types = [
  {
    id: 0,
    label: 'Электронная почта',
    value: 'EMAIL',
  },
  {
    id: 1,
    label: 'Почта',
    value: 'MAIL',
  },
];

export const response_types_en = [
  {
    id: 0,
    label: 'Email',
    value: 'EMAIL',
  },
  {
    id: 1,
    label: 'Post',
    value: 'MAIL',
  },
];

export const response_types_kg = [
  {
    id: 0,
    label: 'Электрондук почта',
    value: 'EMAIL',
  },
  {
    id: 1,
    label: 'Кадимки почта',
    value: 'MAIL',
  },
];

export const initialValuesSchema = {
  // complaint_target: "GO",
  complaint_target: '',
  target_org_name: '',
  // court_process: "YES",
  court_process: '',
  // appeal: "YES",
  appeal: '',
  // one_year: "YES",
  one_year: '',
  // complaint_type: "B",
  complaint_type: '',
  full_name_fiz: '',
  iin: '',
  // document_type: "PATENT",
  document_type: '',
  document_number: '',
  full_name_yur: '',
  head_of_organ: '',
  staff_name: '',
  iin_staff: '',
  reg_number_min_us: '',
  // region: "BISHKEK",
  region: '',
  // activity: "MINING_INDUSTRY",
  activity: '',
  other_activity: '',
  complaint_description: '',
  problem_solve: '',
  our_act: '',
  // is_anonymous: "NO",
  is_anonymous: '',
  reason_of_anonymity: '',
  need_money: '',
  money_amount: '',
  currency: '',
  applicant_name: '',
  address: '',
  telephone_number: '',
  email: '',
  // response_type: "EMAIL",
  response_type: '',

  complaint_files: [],
  term1: false,
  term2: false,
  term3: false,
  term4: false,
};

import React, { useContext } from 'react';
import { LanguageContext } from '../../App';

export default function AboutCompanySection() {
  const langState = useContext(LanguageContext);

  return (
    <div className='about-company' id='about'>
      <div>
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width={36}
          height={36}
          viewBox='0 0 36 36'
        >
          <g
            id='Group_106'
            data-name='Group 106'
            transform='translate(-276.667 -1415.621)'
          >
            <g
              id='Component_1_3'
              data-name='Component 1 – 3'
              transform='translate(276.667 1415.621)'
            >
              <circle
                id='Ellipse_21'
                data-name='Ellipse 21'
                cx={18}
                cy={18}
                r={18}
                fill='#2653ad'
              />
            </g>
            <g id='info' transform='translate(285.543 1424.488)'>
              <ellipse
                id='Ellipse_17'
                data-name='Ellipse 17'
                cx={9}
                cy='8.969'
                rx={9}
                ry='8.969'
                transform='translate(0.002)'
                fill='none'
                stroke='#fff'
                strokeLinecap='round'
                strokeLinejoin='round'
                strokeWidth={2}
              />
              <line
                id='Line_13'
                data-name='Line 13'
                y1='3.588'
                transform='translate(9.306 8.662)'
                fill='none'
                stroke='#fff'
                strokeLinecap='round'
                strokeLinejoin='round'
                strokeWidth={2}
              />
              <line
                id='Line_14'
                data-name='Line 14'
                transform='translate(9.306 5.074)'
                fill='none'
                stroke='#fff'
                strokeLinecap='round'
                strokeLinejoin='round'
                strokeWidth={2}
              />
            </g>
          </g>
        </svg>
      </div>
      {langState.lang === 'ru' ? (
        <div className='about-company-text'>
          <p>
            Бизнес-омбудсмен рассматривает жалобы субъектов предпринимательства
            на действия, решения или бездействие государственных органов,
            органов местного самоуправления, предприятий государственного
            сектора, их должностных лиц и проводит собственную, независимую,
            экспертную и справедливую оценку жалоб и обращений предпринимателей.
            <br /> <br />
            Бизнес-омбудсмен осуществляет свою деятельность на основании
            <a
              style={{ color: 'var(--primary)' }}
              target='_blank'
              rel='noreferrer'
              href='http://cbd.minjust.gov.kg/act/view/ru-ru/12866'
            >
              {' '}
              Постановления №647 Правительства Кыргызской Республики “Об
              уполномоченном лице по защите прав, свобод и законных интересов
              субъектов предпринимательской деятельности” от 31 декабря 2018
              года
            </a>
          </p>

          <p>
            Наблюдательный совет – высший орган управления Бизнес-омбудсмена и
            секретариата, образованный группой сторон для осуществления надзора
            за деятельностью Бизнес-омбудсмена и секретариата.
            <br />
            <br />
            Наблюдательный совет состоит из уполномоченных представителей трех
            сторон:
            <br />
            <br />
            <ul>
              <li>
                сторона 1 – Правительство Кыргызской Республики (1 голос);
              </li>
              <br />
              <li>сторона 2 – международные партнеры по развитию (1 голос);</li>
              <br />
              <li>сторона 3 – бизнес-ассоциации (1 голос).</li>
            </ul>
            <br />В 2022 году в Наблюдательный совет Бизнес-омбудсмена входят:
            <ul>
              <li>
                от Правительства Кыргызской Республики – Министерство экономики
                и коммерции Кыргызской Республики;
              </li>
              <br />
              <li>
                {' '}
                от международных партнеров по развитию – Европейский банк
                реконструкции и развития;
              </li>
              <br />
              <li>
                от бизнес-сообщества – на постоянной основе:
                Торгово-промышленная палата Кыргызской Республики, Американская
                торговая палата в Кыргызской Республике, бизнес-ассоциация ЖИА,
                Международный деловой совет, Ассоциация рынков, предприятий
                торговли и сферы услуг Кыргызстана. На ротационной основе:
                Ассоциация операторов связи и Ассоциация предприятий легкой
                промышленности «Легпром».
              </li>
            </ul>
          </p>
        </div>
      ) : langState.lang === 'kg' ? (
        <div className='about-company-text'>
          <p>
            Бизнес-акыйкатчы мамлекеттик органдардын, жергиликтүү ѳзүн-ѳзү
            башкаруу органдарынын жана мамлекеттик сектордун ишканаларынын
            иш-аракеттерине (чечимдери кошулуп) же болбосо аракетсиздигине
            карата бизнес субъектилеринин даттанууларын карап чыгат жана
            ишкерлерден түшкѳн арыздар менен кайрылууларга жеке кѳз карандысыз,
            эксперттик жана адилеттүү баасын берет.
            <br /> <br />
            Кѳз карандысыз отчётторубуз менен сунуштарыбыз аркылуу биз
            Кыргызстандын бизнес жүргүзүү жана инвестицияларды салуу үчүн
            уникалдуу жер катары калыптануусу үчүн бизнес, ѳкмѳт жана мамлекет
            тарабынан жумшалган күч-аракеттерди чыӊдайбыз.
            <br /> <br />
            Бизнес-акыйкатчы
            <a
              style={{ color: 'var(--primary)' }}
              target='_blank'
              rel='noreferrer'
              href='http://cbd.minjust.gov.kg/act/view/ru-ru/12866'
            >
              {' '}
              «Ишкердик иштин субъектилеринин укуктарын, эркиндиктерин жана
              мыйзамдуу кызыкчылыктарын коргоо боюнча ыйгарым укуктуу жак
              жѳнүндѳ» Кыргыз Республикасынын Ѳкмѳтүнүн 2018-жылдын
              31-декабрындагы №647-Токтомунун негизинде түзүлгѳн.
            </a>
            <br />
            <br />
            2022-жылы Бизнес-акыйкатчынын Байкоочу кеңешинин курамына төмөнкүлөр
            кирет:
            <br />
            <ul>
              <li>
                Кыргыз Республикасынын Өкмөтүнөн - Кыргыз Республикасынын
                Экономика жана коммерция министрлиги;
              </li>
              <br />
              <li>
                {' '}
                өнүктүрүү боюнча эл аралык өнөктөштөрдөн – Европа
                реконструкциялоо жана өнүктүрүү банкы;
              </li>
              <br />
              <li>
                бизнес-коомчулуктан - туруктуу негизде: Кыргыз Республикасынын
                Соода-өнөр жай палатасы, Кыргыз Республикасындагы Америкалык
                Соода палатасы, ЖИА бизнес- ассоциациясы, Эл аралык бизнес
                кеңеши, Кыргызстандын базарлар, соода ишканалары жана кызмат
                көрсөтүү ассоциациясы. Ротациялык негизде: Байланыш
                операторлорунун ассоциациясы жана «Легпром» жеңил өнөр жай
                ишканаларынын ассоциациясы.
              </li>
            </ul>
          </p>
        </div>
      ) : (
        <div className='about-company-text'>
          <p>
            The Business Ombudsman - an authorized person to protect the rights,
            freedoms, and legitimate interests of business entities. The
            Business Ombudsman protects the rights of private businesses and
            entrepreneurs, defending them with legitimate claims against the
            state or sub-state entities and local self-government bodies that
            infringe on their rights.
            <br /> <br />
            The Business Ombudsman of the Kyrgyz Republic acts on the basis of
            <a
              style={{ color: 'var(--primary)' }}
              target='_blank'
              rel='noreferrer'
              href='http://cbd.minjust.gov.kg/act/view/ru-ru/12866'
            >
              {' '}
              Resolution No. 647 of the Government of the Kyrgyz Republic from
              December 31, 2018
            </a>
            <br /> <br />
            The governing collegial body of the Business Ombudsman and his
            secretariat is the Supervisory Board, organized by the group of
            parties for the purpose of oversight of the Business Ombudsman’s and
            secretariat’s activities.
            <br /> <br />
            The Supervisory Board consists of authorized representatives of 3
            parties (together - a group of parties):
            <br /> <br />
            Party 1 - Government of the Kyrgyz Republic (1 vote); Party 2 -
            international development partners (1 vote); Party 3 - business
            associations (1 vote)
            <br /> <br />
            In 2022, the members of the Supervisory Board are:
            <br />• from the Government of the Kyrgyz Republic - the Ministry of
            Economy and Commerce of the Kyrgyz Republic;
            <br />• from international development partners - European Bank for
            Reconstruction and Development;
            <br />• from business associations - on a permanent basis: Chamber
            of Commerce and Industry of the Kyrgyz Republic, American Chamber of
            Commerce in the Kyrgyz Republic, JIA Business Association,
            International Business Council, Association of Markets, Trade and
            Services Enterprises of Kyrgyzstan. On a rotating basis -
            Association of Telecom Operators and Association of Light Industry
            Enterprises "Legprom".
          </p>
        </div>
      )}
    </div>
  );
}

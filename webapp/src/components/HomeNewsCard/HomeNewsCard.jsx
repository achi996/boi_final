import React, { useContext } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { Link } from 'react-router-dom';
import { LanguageContext } from '../../App';
import { formatDate } from '../../_helpers/date';
import Truncate from 'react-truncate';

export default function HomeNewsCard({ data }) {
  const { date, title, description, id } = data;
  const langState = useContext(LanguageContext);

  return (
    <div className="single_home_new">
      <p className="single_home_new_date standart_p">
        {formatDate(date.split('-'))}
      </p>
      <p className="standart_p single_home_new_title">
        <Link to={`/${langState.lang}/news/${id}`}>{title}</Link>
      </p>
      <Truncate
        className="standart_p single_home_new_text"
        style={{ marginTop: 10 }}
        lines={2}
        ellipsis={<span>...</span>}
      >
        {ReactHtmlParser(description)}
      </Truncate>
    </div>
  );
}

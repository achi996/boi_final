export const truncateText = (text, maxLengthBeforeTruncate) =>
  text?.length > maxLengthBeforeTruncate
    ? text.slice(0, maxLengthBeforeTruncate - 3) + '...'
    : text;

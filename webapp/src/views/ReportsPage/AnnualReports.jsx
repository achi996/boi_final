import { Grid, Paper } from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BaseLayout from '../../shared/Layout';
import styles from './ReportsPage.module.css';
import axios from 'axios';
import Loader from '../../components/UI/Loader/Loader';
import { formatDate } from '../../_helpers/date';
import { LanguageContext } from '../../App';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));

const { REACT_APP_API_URL } = process.env;

export default function AnnualReports() {
  const [reports, setReports] = useState(null);
  const langState = useContext(LanguageContext);

  const classes = useStyles();

  useEffect(() => {
    const fetchReports = async () => {
      const {
        data: { results },
      } = await axios(`${REACT_APP_API_URL}/${langState.lang}/annual-reports/`);

      return results;
    };
    fetchReports().then((results) => setReports(results));
  }, [langState.lang]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />

      <div className={styles.root}>
        <h1 style={{ marginBottom: 25 }}>
          {langState.lang === 'ru'
            ? 'Годовые отчеты'
            : langState.lang === 'kg'
            ? 'Жылдык отчёттор'
            : 'Annual reports'}
        </h1>

        <Grid container spacing={3} className={styles.container}>
          <Grid item xs={12}>
            {reports ? (
              reports.map((report, index) => (
                <Paper
                  className={classes.paper}
                  style={{ marginBottom: 25 }}
                  key={index}
                >
                  <p
                    className="single-new-date standart_p"
                    style={{ marginBottom: 10 }}
                  >
                    {formatDate(report.date.split('-'))}
                  </p>
                  <p
                    className="standart_p single-new-title"
                    style={{ marginBottom: 10 }}
                  >
                    <a href={report.file} target="blank">
                      {report.title}
                    </a>
                  </p>
                </Paper>
              ))
            ) : (
              <Loader />
            )}
          </Grid>
        </Grid>
      </div>
    </BaseLayout>
  );
}

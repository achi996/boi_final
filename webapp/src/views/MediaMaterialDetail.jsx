import React, { useContext, useEffect, useState } from 'react';
import BaseLayout from '../shared/Layout';
import Slider from 'react-slick';
import styles from '../components/UI/Slider.module.css';
import ReactHtmlParser from 'react-html-parser';
import { LanguageContext } from '../App';
import axios from 'axios';
import Loader from '../components/UI/Loader/Loader';
import { formatDate } from '../_helpers/date';
import history from '../_helpers/history';

const settings = {
  dots: true,
  infinite: true,
  speed: 600,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 10000,
  cssEase: 'ease-out',
  pauseOnHover: false,
};

const { REACT_APP_API_URL } = process.env;

export default function MediaMaterialDetail(props) {
  const [mediaMaterialDetail, setMediaMaterialDetail] = useState(null);
  const langState = useContext(LanguageContext);
  const mediaMaterialId = props.match.params.id;

  const fetchMediaMaterialDetail = async (url) => {
    try {
      const response = await axios.get(url);
      const data = response.data;
      setMediaMaterialDetail(data);
    } catch (e) {
      if (e.response.status === 404) {
        history.push('/notfound');
      }
    }
  };

  useEffect(() => {
    const url = `${REACT_APP_API_URL}/${langState.lang}/media/${mediaMaterialId}/`;
    fetchMediaMaterialDetail(url);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [langState.lang]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />

      <section className="news_cases_content">
        <div className="news-section active">
          {mediaMaterialDetail ? (
            <div className="single-new">
              <p className="single-new-date standart_p">
                {formatDate(mediaMaterialDetail.date.split('-'))}
              </p>
              <p
                className="standart_p single-new-title"
                style={{ marginBottom: '20px' }}
              >
                {mediaMaterialDetail.title}
              </p>
              <Slider {...settings} className={{ cursor: 'pointer' }}>
                <img
                  className={styles.heroImg}
                  src={mediaMaterialDetail.image}
                  alt={mediaMaterialDetail.title}
                />
              </Slider>

              <br />
              <p
                className="standart_p single-new-text"
                style={{ marginTop: '20px' }}
              >
                {ReactHtmlParser(mediaMaterialDetail.description)}
              </p>
            </div>
          ) : (
            <Loader />
          )}
        </div>
      </section>
    </BaseLayout>
  );
}

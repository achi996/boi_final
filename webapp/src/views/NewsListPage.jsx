import React, { useContext, useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import styles from '../views/ReportsPage/ReportsPage.module.css';
import axios from 'axios';
import { formatDate } from '../_helpers/date';
import Loader from '../components/UI/Loader/Loader';
import ReactHtmlParser from 'react-html-parser';
import Truncate from 'react-truncate';
import { LanguageContext } from '../App';
import BaseLayout from '../shared/Layout';
import history from '../_helpers/history';

const { REACT_APP_API_URL } = process.env;

export default function NewsListPage() {
  const langState = useContext(LanguageContext);
  const [news, setNews] = useState(null);
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(null);

  useEffect(() => {
    const fetchNews = async (url) => {
      const { data } = await axios.get(url);
      return data;
    };
    history.replace(`/${langState.lang}/news`);
    const url = `${REACT_APP_API_URL}/${langState.lang}/news/?limit=20&offset=${
      page * 20 - 20
    }`;
    fetchNews(url).then((data) => {
      setCount(Math.ceil(data.count / 20));
      setNews(data.results);
    });
  }, [langState.lang, page]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />

      <section className="news_cases_content">
        <div
          className={['news-section', 'active', styles.root].join(' ')}
          style={{ display: 'flex' }}
        >
          <h1>
            {langState.lang === 'ru'
              ? 'Новости'
              : langState.lang === 'kg'
              ? 'Жаңылыктар'
              : 'News'}
          </h1>

          {news ? (
            <div>
              {news.map((singleNew, index) => (
                <div className="single-new" key={index}>
                  <p className="single-new-date standart_p">
                    {formatDate(singleNew.date.split('-'))}
                  </p>
                  <p className="standart_p single-new-title">
                    <NavLink to={`/${langState.lang}/news/${singleNew.id}`}>
                      {singleNew.title}
                    </NavLink>
                  </p>
                  <Truncate
                    className="standart_p single-new-text"
                    style={{ marginTop: 10 }}
                    lines={2}
                    ellipsis={<span>...</span>}
                  >
                    {ReactHtmlParser(singleNew.description)}
                  </Truncate>
                </div>
              ))}

              <Pagination
                count={count}
                onChange={(e, value) => setPage(value)}
                page={page}
                color="primary"
                style={{ marginTop: 30 }}
              />
            </div>
          ) : (
            <Loader />
          )}
        </div>
      </section>
    </BaseLayout>
  );
}

import React, { useContext, useEffect, useState } from 'react';
import Slider from 'react-slick';
import history from '../_helpers/history';
import BaseLayout from '../shared/Layout';
import styles from '../components/UI/Slider.module.css';
import axios from 'axios';
import ReactHtmlParser from 'react-html-parser';
import Loader from '../components/UI/Loader/Loader';
import { formatDate } from '../_helpers/date';
import reportsStyles from '../views/ReportsPage/ReportsPage.module.css';
import { LanguageContext } from '../App';

const settings = {
  dots: true,
  infinite: true,
  speed: 600,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 10000,
  cssEase: 'ease-out',
  pauseOnHover: false,
};

const { REACT_APP_API_URL } = process.env;

export default function NewsDetailPage(props) {
  const [newsDetail, setNewsDetail] = useState(null);
  const [loading, setLoading] = useState(false);
  const newsId = props.match.params.id;

  const langState = useContext(LanguageContext);

  const fetchNewsDetail = async (url) => {
    try {
      const response = await axios.get(url);
      const data = response.data;
      setNewsDetail(data);
      history.replace(`/${langState.lang}/news/${data.id}`);
    } catch (e) {
      if (e.response.status === 404) {
        history.push('/notfound');
      }
    }
  };

  console.log('rerender!');

  useEffect(() => {
    setLoading(true);
    let url;
    if (langState.lang === 'en' && newsDetail?.en_version) {
      url = `${REACT_APP_API_URL}/${langState.lang}/news/${newsDetail?.en_version}/`;
    } else if (langState.lang === 'ru' && newsDetail?.ru_version) {
      url = `${REACT_APP_API_URL}/${langState.lang}/news/${newsDetail?.ru_version}/`;
    } else if (langState.lang === 'kg' && newsDetail?.kg_version) {
      url = `${REACT_APP_API_URL}/${langState.lang}/news/${newsDetail?.kg_version}/`;
    } else {
      url = `${REACT_APP_API_URL}/${langState.lang}/news/${newsId}/`;
    }
    fetchNewsDetail(url).then(() => setLoading(false));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [langState.lang]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />
      <section className="news_cases_content">
        <div
          className={['news-section', 'active', reportsStyles.root].join(' ')}
        >
          {loading ? <h1>Загрузка...</h1> : null}
          {newsDetail ? (
            <div className="single-new" style={{ paddingTop: 0 }}>
              <p className="single-new-date standart_p">
                {formatDate(newsDetail.date.split('-'))}
              </p>
              <p
                className="standart_p single-new-title"
                style={{ marginBottom: '20px' }}
              >
                {newsDetail.title}
              </p>
              <Slider {...settings} className={{ cursor: 'pointer' }}>
                {newsDetail.images.map((item) => (
                  <img
                    className={styles.heroImg}
                    src={item.image}
                    alt={`Изображение: ${newsDetail.title}`}
                  />
                ))}
              </Slider>
              <br />
              <p
                className="standart_p single-new-text"
                style={{ marginTop: '20px' }}
              >
                {ReactHtmlParser(newsDetail.description)}
              </p>

              {newsDetail.videos.length ? (
                <div>
                  <p className="single_new_media_title">Видеоматериалы</p>
                  <div className="single_new_videos">
                    {newsDetail.videos.map((video) => (
                      <div
                        className="single_new_video"
                        key={video.id}
                        dangerouslySetInnerHTML={{
                          __html: video.link,
                        }}
                      />
                    ))}
                  </div>
                </div>
              ) : null}

              {newsDetail.pdfs.length ? (
                <div>
                  <p className="single_new_media_title">Документы</p>
                  <div className="singe_new_pdfs">
                    {newsDetail.pdfs.map((pdf) => (
                      <a href={pdf.file} download>
                        {pdf.file.split('/').pop()}
                      </a>
                    ))}
                  </div>
                </div>
              ) : null}
            </div>
          ) : (
            <Loader />
          )}
        </div>
      </section>
    </BaseLayout>
  );
}

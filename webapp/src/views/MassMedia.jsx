import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import BaseLayout from '../shared/Layout';
import styles from '../views/ReportsPage/ReportsPage.module.css';
import axios from 'axios';
import { formatDate } from '../_helpers/date';
import ReactHtmlParser from 'react-html-parser';
import Truncate from 'react-truncate';
import Loader from '../components/UI/Loader/Loader';
import { LanguageContext } from '../App';

export default function MassMedia() {
  const [massMediaList, setMassMediaList] = useState(null);
  const { REACT_APP_API_URL } = process.env;
  const langState = useContext(LanguageContext);

  const fetchMassMedia = async (url) => {
    const response = await axios.get(url);
    const data = response.data.results;
    console.log(data);
    setMassMediaList(data);
  };

  useEffect(() => {
    const url = `${REACT_APP_API_URL}/${langState.lang}/smi/`;
    fetchMassMedia(url);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [langState.lang]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />
      <section className="news_cases_content">
        <div
          className={['news-section', 'active', styles.root].join(' ')}
          style={{ display: 'flex' }}
        >
          <h1>
            {langState.lang === 'ru'
              ? 'СМИ о нас'
              : langState.lang === 'kg'
              ? 'СМИ биз жѳнүндѳ'
              : 'Media about us'}
          </h1>

          {massMediaList ? (
            massMediaList.map((massMedia, index) => (
              <div className="single-new" key={index}>
                <p className="single-new-date standart_p">
                  {formatDate(massMedia.date.split('-'))}
                </p>
                <p className="standart_p single-new-title">
                  <Link to={`/mass-media/${massMedia.id}`}>
                    {massMedia.title}
                  </Link>
                </p>
                <p
                  className="standart_p single-new-text"
                  style={{ marginTop: 10 }}
                >
                  <Truncate lines={2} ellipsis={<span>...</span>}>
                    {ReactHtmlParser(massMedia.description)}
                  </Truncate>
                </p>
              </div>
            ))
          ) : (
            <Loader />
          )}
        </div>
      </section>
    </BaseLayout>
  );
}

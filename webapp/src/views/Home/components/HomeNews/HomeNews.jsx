import React, { useContext, useEffect, useState } from 'react';
import classes from './HomeNews.module.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { LanguageContext } from '../../../../App';
import HomeNewsCard from '../../../../components/HomeNewsCard';
import MainNewsCard from '../MainNewsCard/MainNewsCard';

export default function HomeNews() {
  const [mainNews, setMainNews] = useState(null);
  const { REACT_APP_API_URL } = process.env;
  const langState = useContext(LanguageContext);

  useEffect(() => {
    const fetchMainNews = async () => {
      const response = await axios(
        `${REACT_APP_API_URL}/${langState.lang}/news/?is_main=true&limit=7`
      );
      const data = response.data.results;
      setMainNews(data);
    };

    fetchMainNews();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [langState.lang]);

  return (
    <>
      <div
        className="mission-icon"
        style={{ display: 'flex', justifyContent: 'center' }}
      >
        {langState.lang === 'ru' ? (
          <h2 id="goals">Новости</h2>
        ) : langState.lang === 'kg' ? (
          <h2 id="goals">Жаңылыктар</h2>
        ) : (
          <h2 id="goals">News</h2>
        )}
      </div>

      <section
        className="news_cases_content"
        style={{ marginTop: 0, marginBottom: 0 }}
      >
        <div className="home_news_section active" style={{ display: 'flex' }}>
          {mainNews ? (
            <React.Fragment>
              <MainNewsCard data={mainNews[0]} />
              <div className="home_news_list">
                {mainNews.slice(1, 5).map((news) => (
                  <HomeNewsCard key={news.id} data={news} />
                ))}
              </div>
            </React.Fragment>
          ) : null}
        </div>
      </section>

      <Link to={`/${langState.lang}/news`}>
        <button className={classes.btn}>
          {langState.lang === 'ru'
            ? 'Посмотреть еще'
            : langState.lang === 'kg'
            ? 'Көбүрөөк көрүү'
            : 'See more'}
        </button>
      </Link>
    </>
  );
}

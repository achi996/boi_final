import React, { useContext } from 'react';
import { LanguageContext } from '../../../../App';
import history from '../../../../_helpers/history';

export default function MainNewsCard({ data }) {
  const { images, title, date, id } = data;

  const langState = useContext(LanguageContext);

  return (
    <div
      onClick={() => history.push(`/${langState.lang}/news/${id}`)}
      className={'main_news_card'}
    >
      <p>{date}</p>
      <img src={images[0]?.image} alt={title} />
      <p className={'main_news_card_title'}>{title}</p>
    </div>
  );
}

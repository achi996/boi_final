import React from 'react';
import Hero from './components/Hero';
import ComplaintSection from './components/ComplaintSlider';
import BaseLayout from '../../shared/Layout';
import HomeNews from './components/HomeNews';

export default function Home() {
  return (
    <BaseLayout>
      <Hero />
      <HomeNews />
      <ComplaintSection />
    </BaseLayout>
  );
}

import React, { useContext, useEffect, useState } from 'react';
import Pagination from '@material-ui/lab/Pagination';
import styles from '../views/ReportsPage/ReportsPage.module.css';
import axios from 'axios';
import Loader from '../components/UI/Loader/Loader';
import { LanguageContext } from '../App';
import BaseLayout from '../shared/Layout';
import history from '../_helpers/history';
import AnalyticsCard from '../components/AnalyticsCard';

const { REACT_APP_API_URL } = process.env;

export default function AnalyticsListPage() {
  const langState = useContext(LanguageContext);
  const [analytics, setAnalytics] = useState(null);
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(null);

  useEffect(() => {
    const fetchAnalytics = async (url) => {
      const { data } = await axios.get(url);
      return data;
    };

    history.push(`/${langState.lang}/analytics`);

    const url = `${REACT_APP_API_URL}/${
      langState.lang
    }/analytics/?limit=20&offset=${page * 20 - 20}`;

    fetchAnalytics(url).then((data) => {
      setCount(Math.ceil(data.count / 20));
      setAnalytics(data.results);
    });
  }, [langState.lang, page]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />

      <section className="news_cases_content">
        <div
          className={['news-section', 'active', styles.root].join(' ')}
          style={{ display: 'flex' }}
        >
          <h1>
            {langState.lang === 'ru'
              ? 'Аналитика'
              : langState.lang === 'kg'
              ? 'Аналитика'
              : 'Analytics'}
          </h1>

          {analytics ? (
            <div>
              {analytics.map((analytics) => (
                <AnalyticsCard
                  key={analytics.id}
                  analytics={analytics}
                  langState={langState}
                />
              ))}

              <Pagination
                count={count}
                onChange={(e, value) => setPage(value)}
                page={page}
                color="primary"
                style={{ marginTop: 30 }}
              />
            </div>
          ) : (
            <Loader />
          )}
        </div>
      </section>
    </BaseLayout>
  );
}

import React, { useContext } from 'react';
import BaseLayout from '../../../shared/Layout';
import { LanguageContext } from '../../../App';
import RusEl from './Svgs/RusEl';
import KgEl from './Svgs/KgEl';
import EnEl from './Svgs/EnEl';
import FullRus from './Svgs/FullRus';
import FullEn from './Svgs/FullEn';
import FullKg from './Svgs/FullKg';

export default function Eligibility() {
  const langState = useContext(LanguageContext);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />
      <div className="eligibility-phone-section">
        {langState.lang === 'ru' ? (
          <FullRus />
        ) : langState.lang === 'kg' ? (
          <FullKg />
        ) : (
          <FullEn />
        )}
      </div>
      <div style={{ height: 'auto' }} className="eligibility-section">
        <div
          className={[
            'eligibility-wrapper',
            `${langState.lang === 'en' ? 'eligibility-wrapper-en' : ''}`,
          ].join(' ')}
        >
          <h1 id="eligibility-header">
            {langState.lang === 'ru'
              ? 'КРИТЕРИИ ЖАЛОБ, РАССМАТРИВАЕМЫХ ИНСТИТУТОМ БИЗНЕС-ОМБУДСМЕНА'
              : langState.lang === 'kg'
              ? 'КАНДАЙ АРЫЗ-ДАТТАНУУЛАР БИЗНЕС-АКЫЙКАТЧЫ МЕНЕН КАРАЛАТ'
              : 'COMPLAINT ELIGIBILITY CRITERIA'}
          </h1>
          <p>
            {langState.lang === 'ru'
              ? 'Вы подаете жалобу на государственный орган, орган местного самоуправления, предприятие госсектора или должностное лицо?'
              : langState.lang === 'kg'
              ? 'Сиздин арызыңыз мамлекеттик органдардын аракеттерине же аракетсиздиктерине тийешелүүбү?'
              : 'Is your complaint from private business for actions (including decisions) or inactions of state authorities and local self-goverment bodies, as well as state-owned enterprises and their officials?'}
          </p>
          <p>
            {langState.lang === 'ru'
              ? 'Предмет вашей жалобы находится в процессе судебного/арбитражного рассмотрения или было ли уже принято судебное/ арбитражное решение?'
              : langState.lang === 'kg'
              ? 'Сиздин арызыңыз соттун же арбитраждын териштирүү мезгилинде каралдыбы же соттун чечими (токтому, аныктамасы) барбы?'
              : 'Is your complaint a subject to any court or arbitral proceedings or does it have court decision, arbital award or similar type of decision?'}
          </p>

          <p>
            {langState.lang === 'ru'
              ? 'Прошел ли один год с момента нарушения Ваших прав, свобод или законных интересов?'
              : langState.lang === 'kg'
              ? 'Арыз-даттануунун маселеси акыркы бир жылдын ичинде орун алдыбы?'
              : 'Has it been 1 year since the violation of your business rights?'}
          </p>
          <p>
            {langState.lang === 'ru'
              ? 'Была ли Вами использована возможность административного обжалования хотя бы в одной инстанции?'
              : langState.lang === 'kg'
              ? 'Арыздын маселесин чечүүгѳ жок дегенде бир административдик түрдѳѳ аракет кылдыңызбы?'
              : 'Have you used at least one instance of administrative appeal process available?'}
          </p>
          <h1 id="eligibility-footer">
            {langState.lang === 'ru'
              ? 'ВАША ЗАЯВКА СООТВЕТСТВУЕТ КРИТЕРИЯМ ИНСТИТУТА БИЗНЕС-ОМБУДСМЕНА'
              : langState.lang === 'kg'
              ? 'СИЗДИН АРЫЗ БИЗНЕС-АКЫЙКАТЧЫ ИНСТИТУТУН ТАЛАПТАРГА ЖООП БЕРЕТ'
              : 'YOUR APPLICATION MEETS THE CRITERIA OF BUSINESS OMBUDSMAN INSTITUTE'}
          </h1>
        </div>
        {langState.lang === 'ru' ? (
          <RusEl
            className="eligibility-img-wrapper"
            style={{ height: '100%' }}
          />
        ) : langState.lang === 'kg' ? (
          <KgEl />
        ) : (
          <EnEl />
        )}
      </div>
    </BaseLayout>
  );
}

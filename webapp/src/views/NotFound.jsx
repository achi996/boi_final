import React from 'react';
import BaseLayout from '../shared/Layout';

export default function NotFound() {
  return (
    <BaseLayout>
      <div style={{ height: 100 }}></div>

      <div className="not_found">
        <h1>404</h1>

        <h2>Страница не найдена!</h2>
      </div>
    </BaseLayout>
  );
}

import React, { useContext, useEffect, useState } from 'react';
import classes from './ContactsPage.module.css';
import { LanguageContext } from '../../App';
import BaseLayout from '../../shared/Layout';
import axios from 'axios';

const { REACT_APP_API_URL } = process.env;

export default function ContactsPage() {
  const langState = useContext(LanguageContext);
  const [contactsData, setContactsData] = useState(null);

  useEffect(() => {
    const fetchContactsData = async () => {
      try {
        const response = await axios.get(
          `${REACT_APP_API_URL}/${langState.lang}/contact-info/`
        );
        setContactsData(response.data.results[0]);
      } catch (e) {
        console.log('error: ', e.message);
      }
    };

    fetchContactsData();
  }, [langState.lang]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }}></div>
      {contactsData ? (
        <div className={classes.container}>
          {langState.lang === 'ru' || langState.lang === 'kg' ? (
            <>
              <p className={classes.contacts}>
                Тел:{' '}
                <a href={`tel:${contactsData.phone1}`}>
                  {contactsData.phone1},
                </a>{' '}
                <a href={`tel:${contactsData.phone2}`}>
                  {contactsData.phone2},
                </a>
              </p>

              <p>
                Email: <a href="mailto:aryz@boi.kg">aryz@boi.kg</a>
              </p>

              <p>
                {langState.lang === 'ru'
                  ? `Адрес: ${contactsData.city}, ${contactsData.street} ${contactsData.number}, ${contactsData.office}`
                  : langState.lang === 'kg'
                  ? `Дарек: ${contactsData.city}, ${contactsData.street} ${contactsData.number}, ${contactsData.office}`
                  : `Address: ${contactsData.city}, ${contactsData.street} ${contactsData.number}, ${contactsData.office}`}
              </p>
            </>
          ) : (
            <>
              <p className={classes.contacts}>
                Tel:{' '}
                <a href={`tel:${contactsData.phone1}`}>
                  {contactsData.phone1},
                </a>{' '}
                <a href={`tel:${contactsData.phone2}`}>
                  {contactsData.phone2},
                </a>
              </p>

              <p>
                Email: <a href="mailto:aryz@boi.kg">aryz@boi.kg</a>
              </p>

              <p>
                Address: {contactsData.number} {contactsData.street},{' '}
                {contactsData.office}, {contactsData.city} <br />
              </p>
            </>
          )}
        </div>
      ) : null}
    </BaseLayout>
  );
}

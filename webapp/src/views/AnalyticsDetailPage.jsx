/* eslint-disable react/jsx-no-target-blank */
import React, { useContext, useEffect, useState } from 'react';
import BaseLayout from '../shared/Layout';
import axios from 'axios';
import ReactHtmlParser from 'react-html-parser';
import Loader from '../components/UI/Loader/Loader';
import { formatDate } from '../_helpers/date';
import reportsStyles from '../views/ReportsPage/ReportsPage.module.css';
import { LanguageContext } from '../App';
import history from '../_helpers/history';

const { REACT_APP_API_URL } = process.env;

export default function AnalyticsDetailPage(props) {
  const [analyticsDetail, setAnalyticsDetail] = useState(null);
  const [loading, setLoading] = useState(false);
  const newsId = props.match.params.id;

  const langState = useContext(LanguageContext);

  const fetchAnalyticsDetail = async (url) => {
    try {
      const response = await axios.get(url);
      const data = response.data;
      setAnalyticsDetail(data);
      history.push(`/${langState.lang}/analytics/${data.id}`);
    } catch (e) {
      if (e.response?.status === 404) {
        history.push('/notfound');
      }
    }
  };

  useEffect(() => {
    setLoading(true);
    let url;
    if (langState.lang === 'en' && analyticsDetail?.en_version) {
      url = `${REACT_APP_API_URL}/${langState.lang}/analytics/${analyticsDetail?.en_version}/`;
    } else if (langState.lang === 'ru' && analyticsDetail?.ru_version) {
      url = `${REACT_APP_API_URL}/${langState.lang}/analytics/${analyticsDetail?.ru_version}/`;
    } else if (langState.lang === 'kg' && analyticsDetail?.kg_version) {
      url = `${REACT_APP_API_URL}/${langState.lang}/analytics/${analyticsDetail?.kg_version}/`;
    } else {
      url = `${REACT_APP_API_URL}/${langState.lang}/analytics/${newsId}/`;
    }
    fetchAnalyticsDetail(url).then(() => setLoading(false));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [langState.lang]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />
      <section className='news_cases_content'>
        <div
          className={['news-section', 'active', reportsStyles.root].join(' ')}
        >
          {loading ? <h1>Загрузка...</h1> : null}
          {analyticsDetail ? (
            <div className='single-new' style={{ paddingTop: 0 }}>
              <p className='single-new-date standart_p'>
                {formatDate(analyticsDetail.date.split('-'))}
              </p>
              <p
                className='standart_p single-new-title'
                style={{ marginBottom: '20px' }}
              >
                {analyticsDetail.title}
              </p>
              <br />
              <p
                className='standart_p single-new-text'
                style={{ marginTop: '20px' }}
              >
                {ReactHtmlParser(analyticsDetail.description)}
              </p>
              <a
                target='_blank'
                rel='noreferrer noopener'
                download
                href={analyticsDetail.file}
              >
                <p>Скачать материал</p>
              </a>
            </div>
          ) : (
            <Loader />
          )}
        </div>
      </section>
    </BaseLayout>
  );
}

import React from 'react';
import OurTeamSection from '../../../components/OurTeamSection/OurTeamSection';
import BaseLayout from '../../../shared/Layout';

export default function OurTeam() {
  return (
    <BaseLayout>
      <div style={{ height: 100 }}></div>
      <OurTeamSection />
    </BaseLayout>
  );
}

import React, { useContext, useEffect, useState } from 'react';
import NormativeDoc from '../../../components/NormativeDoc/NormativeDoc';
import BaseLayout from '../../../shared/Layout';
import axios from 'axios';
import { LanguageContext } from '../../../App';

const { REACT_APP_API_URL } = process.env;

export default function NormativeDocuments() {
  const [documents, setDocuments] = useState([]);
  const langState = useContext(LanguageContext);

  useEffect(() => {
    const fetchDocs = async () => {
      const {
        data: { results },
      } = await axios(
        `${REACT_APP_API_URL}/${langState.lang}/normative-documents/`
      );
      return results;
    };
    fetchDocs().then((results) => setDocuments(results));
  }, [langState.lang]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />
      <section className="documents-section">
        <div className="mission-icon">
          <svg
            className="dark-shadow"
            xmlns="http://www.w3.org/2000/svg"
            width="50"
            height="50"
            viewBox="0 0 50 50"
          >
            <g
              id="Group_111"
              data-name="Group 111"
              transform="translate(-471 -48)"
            >
              <g
                id="Component_1_5"
                data-name="Component 1 – 5"
                transform="translate(471 48)"
              >
                <circle
                  id="Ellipse_21"
                  data-name="Ellipse 21"
                  cx="25"
                  cy="25"
                  r="25"
                  fill="#2653ad"
                />
              </g>
              <g id="file-text" transform="translate(482 58)">
                <path
                  id="Path_16"
                  data-name="Path 16"
                  d="M16.545,2H6.509A2.509,2.509,0,0,0,4,4.509V24.58a2.509,2.509,0,0,0,2.509,2.509H21.562a2.509,2.509,0,0,0,2.509-2.509V9.527Z"
                  fill="none"
                  stroke="#fff"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                />
                <path
                  id="Path_17"
                  data-name="Path 17"
                  d="M14,2V9.527h7.527"
                  transform="translate(2.545)"
                  fill="none"
                  stroke="#fff"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                />
                <line
                  id="Line_15"
                  data-name="Line 15"
                  x1="10"
                  transform="translate(9 16)"
                  fill="none"
                  stroke="#fff"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                />
                <line
                  id="Line_16"
                  data-name="Line 16"
                  x1="10"
                  transform="translate(9 21)"
                  fill="none"
                  stroke="#fff"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                />
                <path
                  id="Path_18"
                  data-name="Path 18"
                  d="M10.509,9H8"
                  transform="translate(1.018 1.781)"
                  fill="none"
                  stroke="#fff"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                />
              </g>
            </g>
          </svg>
          <h2>
            {langState.lang === 'ru'
              ? 'Нормативные документы'
              : langState.lang === 'kg'
              ? 'Нормативдик документтер'
              : 'Normative documents'}
          </h2>
        </div>

        <div className="documents-set">
          {documents.map((doc) => (
            <NormativeDoc key={doc.id} title={doc.title} url={doc.document} />
          ))}
        </div>
      </section>
      <div style={{ height: 100 }} />
    </BaseLayout>
  );
}

import React, { useContext, useEffect, useState } from 'react';
import BaseLayout from '../shared/Layout';
import ReactHtmlParser from 'react-html-parser';
import { LanguageContext } from '../App';
import axios from 'axios';
import Loader from '../components/UI/Loader/Loader';
import history from '../_helpers/history';

const { REACT_APP_API_URL } = process.env;

export default function CaseDetalPage(props) {
  const [caseDetail, setCaseDetail] = useState(null);

  const langState = useContext(LanguageContext);

  const caseId = props.match.params.id;

  const fetchCaseDetail = async (url) => {
    try {
      const response = await axios.get(url);
      const data = response.data;
      setCaseDetail(data);
    } catch (e) {
      if (e.response.status === 404) {
        history.push('/notfound');
      }
    }
  };

  useEffect(() => {
    const url = `${REACT_APP_API_URL}/${langState.lang}/cases/${caseId}/`;
    fetchCaseDetail(url);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [langState.lang]);

  return (
    <BaseLayout>
      <div style={{ height: 100 }} />
      <section className="news_cases_content">
        <div className="news-section" style={{ display: 'flex' }}>
          {caseDetail ? (
            <div className="single-new">
              <p className="single-new-date standart_p">{caseDetail.date}</p>
              <p className="standart_p single-new-title">{caseDetail.title}</p>
              <p
                className="standart_p single-new-text"
                style={{ marginTop: '20px', fontWeight: 'normal' }}
              >
                {ReactHtmlParser(caseDetail.description)}
              </p>
            </div>
          ) : (
            <Loader />
          )}
        </div>
      </section>
    </BaseLayout>
  );
}
